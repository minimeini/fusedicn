# Apply BICNET to HCP fMRI Data


## Contents

<!-- MarkdownTOC -->

- 1 Getting Started
    - 1.1 Dependencies
    - 1.2 Prepare Data
- 2 Information About HCP Data
- 3 Exploratory Empirical Analysis
    - 3.1 Pairwise Hypothesis Tesing
    - 3.2 Empirical PCA

<!-- /MarkdownTOC -->



## 1 Getting Started

### 1.1 Dependencies


We will use the `3dmaskdump` function from AFNI to preprocess data. The following commands use conda to [install AFNI](https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/index.html) without root privileges, and also install related R and Python packages.

```bash
conda update conda -n ${ENV}
conda activate ${ENV}

conda install conda-libmamba-solver

conda install -y python pip numpy matplotlib statsmodels pandas psutil joblib pathos scikit-learn freetype --solver libmamba

conda install -y openmotif openmotif-dev gsl xorg-libx11 xorg-libxt xorg-libxext xorg-libxi xorg-libxft xorg-libxcb xorg-libxpm xorg-libxmu libglu glib expat freeglut tcsh ffmpeg netpbm lapack armadillo libgcc-ng llvm-openmp nilearn -c conda-forge --solver libmamba

conda install -y r-base r-essentials r-rcpp r-rcpparmadillo r-afex r-phia r-snow r-nlme r-paran r-brms r-shiny r-shinydashboard r-plotly r-colourpicker r-data.table r-gplots r-rcolorbrewer r-psych r-ggplot2 r-ggridges r-dplyr r-tidyr r-scales r-corrplot r-metafor -c conda-forge --solver libmamba

conda install -y -c conda-forge nipy 

cd
curl -O https://afni.nimh.nih.gov/pub/dist/bin/misc/@update.afni.binaries
tcsh @update.afni.binaries -package linux_centos_7_64 -do_extras
```




### 1.2 Prepare Data


#### Create ROI Masks

[Download](https://www.dropbox.com/scl/fo/scmr8sc0xq04ajdgrmdt0/h?rlkey=frv8r5h9u4tx0ux7nx78vbod4&dl=0) the ROI masks of the [AAL90](https://www.gin.cnrs.fr/en/tools/aal/) atlas. 

Alternatively, if you want to create your own ROI masks from a given atlas, you need to install [FSL](https://fsl.fmrib.ox.ac.uk/). Script `main_extract_roi_mask.sh` illustrates how to create ROI masks from the AAL90 atlas using the `fslmaths` function from FSL. 


```r
> atlas = RNifti::readNifti(<PATH TO ATLAS>)
> str(atlas)
 'niftiImage' num [1:91, 1:109, 1:91] 0 0 0 0 0 0 0 0 0 0 ...
 - attr(*, "pixdim")= num [1:3] 2 2 2
 - attr(*, "pixunits")= chr [1:2] "mm" "s"
 - attr(*, ".nifti_image_ptr")=<externalptr> 
 - attr(*, ".nifti_image_ver")= int 2
```

Usually, the `.nii` atlas is a three-dimensional matrix, the value of each (x,y,z) represents the label of a specific ROI, and voxels within the same value belong to the same ROI. With this information, there are many way to extract a single ROI. For example,
```bash
fslmaths ${nifti_file} -bin -thr ${value_at_xyz} -uthr ${value_at_xyz} ${roi_of_this_value}
```



#### Download and Preprocess Data

We need an acount of the [Connectomedb](https://db.humanconnectome.org/app/template/Login.vm).

Define the list of subjects you want to use and put it under: `${repo}/info/hcp_subject_info.txt`. 

- This file is separated by ` `.
- It must be a text file that has two columns, **sid** and **status** with no column header. 
- The first column **sid** must be the six-digit HCP subject id. 
- The second column **status** is set to 1 if this subject has all the tfMRI recordings and the rfMRI recordings.

Run the following commands:

```bash
repo=<SET PATH TO REPO HERE>
sed -i '' 's|repo="${pfix}/Dropbox/repository/fusedicn"|repo="<ALSO SET PATH TO REPO HERE>"|' "${repo}/main_download_hcp_data.sh"
# ${repo}: Absolute local directory of this repository. Return error if it doesn't exist.

sed -i '' 's|datapath="${pfix}/Dropbox/data"|datapath="<SET PATH TO DATA HERE>"|' "${repo}/main_download_hcp_data.sh"
# ${datapath}: Local directory to save temporary data and preprocessed data. A empty folder will created if it has not already existed.

sed -i '' 's|sublist="${repo}/info/hcp_subject_info.txt"|sublist="<SET SUBLIST HERE>"|' "${repo}/main_download_hcp_data.sh"
# ${sublist}: List of subjects we want to use. A text file that has two columns with no column header. The first column must be the six-digit HCP subject id. This file is separated by ` `.


sed -i '' 's|hcp_id="welkin31"|hcp_id="<SET CONNECTOMDB USERNAME HERE>"|' "${repo}/main_download_hcp_data.sh"
sed -i '' 's|hcp_pswd="hetcube31"|hcp_pswd="<SET CONNECTOMDB PASSWORD HERE>"|' "${repo}/main_download_hcp_data.sh"
# `${hcp_id}` and `${hcp_pswd}`: Your credentials to get access to the [Connectomedb](https://db.humanconnectome.org/app/template/Login.vm).


sed -i '' 's|WORKER_LIMIT=6|WORKER_LIMIT=<SET NUMBER OF CORES HERE>|' "${repo}/main_download_hcp_data.sh"
# `${WORKER_LIMIT}`: Number of cores you want to use for parallel background shells.

chmod +x ${repo}/main_download_hcp_data.sh
${repo}/main_download_hcp_data.sh
```

The preprocssed time series data is `${path_to_data}/{TYPE}/{TASK}/{TYPE}_{TASK}_{PHASE}_AAL90_mean_{SUBJECT ID}.csv`.


## 2 Information About HCP Data


- What we use: HCP_1200 > 3T scan > minimally preprocessed
    + Task fMRI: `tfMRI_LANGUAGE_RL_preproc/MNINonLinear/Results/tfMRI_LANGUAGE_RL/tfMRI_­LANGUAGE_­RL.­nii.­gz` 
    + Resting-state fMRI: `rfMRI_REST1_RL_FIX/MNINonLinear/Results/rfMRI_REST1_RL/tfMRI_­LANGUAGE_­RL_hp2000_clean.nii.gz` 
- Basic Stats
    + Repetition time (TR) = 720ms (0.72s), the length of time between corresponding consecutive points on a repeating series of pulses.
    + Echo time (TE) = 33.1ms, the length of time from the center of the RF-pulse to the center of the echo.
    + Flip angle = 52.
    + voxel spatial resolution = [2.0 2.0 2.0]mm.
    + 3T, LR is the second run (scan order = 2)
    + Recording always started at the first stimulus.
- Experimental design: [Task-fMRI 3T Imaging Protocol Details](http://protocols.humanconnectome.org/HCP/3T/task-fMRI-protocol-details.html)




## 3 Exploratory Empirical Analysis

> Related scripts are in `gitpath/empirical`.

### 3.1 Pairwise Hypothesis Tesing

> WARNING: HUGE COMPUTATION. 
> Run it in the workstation for parallel computing.

1. `main_dynamic_connectivity.py`: Compute time-varying connectivity using sliding window. The connectivity is measured by pearson's sample correlation and z-transformed.
    - Input: `datapath/{TYPE}_roi_mean`
    - Output: `datapath/{TYPE}_connmat`.
    - Running time: ~ 6 secs
2. `main_twosample_test.py`: pairwise independent two-sample t-test or Kolmogorov–Smirnov test.
    - Input: `datapath/{TYPE}_connmat`
    - Output: `datapath/int_test/{ATLAS}/{TEST}_{rfMRI TASK}_{tfMRI TASK}.npy`
    - Running time: ~ 30 secs
3. `eda_pairwise_test.ipynb`: plot connections which are significantly different between task and rest.
    - Results: `eda_pairwise_test.pdf`
    - Script: `main_visualize_test.py`
    - Comments: The results given by the t-test are highly consistent with those given by the Kolmogorov-Smirnov test. Considering the computational efficiency, t-test is recommended.


### 3.2 Empirical PCA

- `eda_set.ipynb`: analysis with the SET dataset
- `eda_hcp_rest.ipynb`: analysis with the HCP rfMRI dataset
- `eda_hcp_language.ipynb`: analysis with the HCP language tfMRI dataset



