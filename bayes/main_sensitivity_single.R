suppressMessages(require(Rcpp))
suppressMessages(require(GIGrvg))
suppressMessages(require(ggplot2))
suppressMessages(require(lattice))
suppressMessages(require(gridExtra))

code_path = "../../spdynfac"

if (Sys.info()[1] == "Darwin") {
	Sys.setenv("PKG_CXXFLAGS"="-std=c++11 -march=native -O2 -Xpreprocessor -fopenmp")
	Sys.setenv("PKG_LIBS"="$(LAPACK_LIBS) $(BLAS_LIBS) $(FLIBS) -fopenmp ")
	sourceCpp(file.path(code_path, "model/sampler.cpp"))
} else if (Sys.info()[1] == "Linux") {
	Sys.setenv("PKG_CXXFLAGS"="-std=c++11 -march=native -O2 -fopenmp")
	Sys.setenv("PKG_LIBS"="$(LAPACK_LIBS) $(BLAS_LIBS) $(FLIBS) -fopenmp -DARMA_NO_DEBUG")
	sourceCpp(file.path(code_path, "model/samplerlin.cpp"))
}


sourceCpp(file.path(code_path, "evaluation/evaluation.cpp"))
source(file.path(code_path,"rutils/utils.R"))
source(file.path(code_path,"evaluation/evaluation.R"))
source(file.path(code_path,"evaluation/visualization.R"))


debug = FALSE

if (debug) {
	if (Sys.info()[1] == "Darwin") {
		rest_path = "/Users/meini/data/rfMRI_roi_mean/AAL90/REST1"
		task_path = "/Users/meini/data/tfMRI_roi_mean/AAL90/LANGUAGE"
	} else if (Sys.info()[1] == "Linux") {
		rest_path = "/ibex/scratch/tangm0a/data/rfMRI_roi_mean/AAL90/REST1"
		task_path = "/ibex/scratch/tangm0a/data/tfMRI_roi_mean/AAL90/LANGUAGE"
	}
	
	rest_pref = "rfMRI_REST1_LR_AAL90_mean_"
	task_pref = "tfMRI_LANGUAGE_LR_AAL90_mean_"
	fmrt = ".csv"

	nsub_max = 2
	varexp = 0.75

	Kest = 74
	nburnin = 0
	niter = 30000
	nthin = 1

	c_sigma = 3
	d_sigma = 0.5
	Bsigma = 0.2
	a_phi = 10
	b_phi = 1
	b_mu = 0
	B_mu = 1
	beta = 1
	sigma2_v = 1
	a_tau = 0.1
	c_lambda = 1
	d_lambda = 1

	dynfac = TRUE
	restricted = FALSE
	interweave = FALSE

	run_fused = TRUE
	run_separate = TRUE
	run_visualize = FALSE

	save_path = "../figures/realdata/"
	sub_list = NULL
}


normalize = function(x) {
	return((x-mean(x,na.rm=TRUE))/sd(x,na.rm=TRUE))
}


main_sensitivity_single = function(
	sid,
	Kest = 18,
	rest_path = "/ibex/scratch/tangm0a/data/rfMRI_roi_mean/AAL90/REST1", # parent folder of resting data
	task_path = "/ibex/scratch/tangm0a/data/tfMRI_roi_mean/AAL90/LANGUAGE", # parent folder of task-related data
	m_pi = NULL,
	nburnin = 100000,
	niter = 900000,
	nthin = 2,
	c_sigma = 2,
	d_sigma = NULL,
	Bsigma = 0.2,
	a_phi = 10,
	b_phi = 2.5,
	b_mu = 0,
	B_mu = 1,
	beta = 1,
	a_tau = 0.1,
	c_lambda = 1,
	d_lambda = 1,
	sub_list = NULL, 
	rest_pref = "rfMRI_REST1_LR_AAL90_mean_",
	task_pref = "tfMRI_LANGUAGE_LR_AAL90_mean_",
	fmrt = ".csv",
	save_path = "../output") {

	if (!is.null(m_pi)) {
		m_pi = rep(m_pi[1],Kest)
	}

	# Yrest: N x Trest
	# Ytask: N x Ttask
	Yrest = as.matrix(read.csv(file.path(rest_path,
		paste0(rest_pref,sid,fmrt)), header=FALSE))
	Ytask = as.matrix(read.csv(file.path(task_path,
		paste0(task_pref,sid,fmrt)), header=FALSE))

	# check if there is any constant channel
	NullChan = matrix(0,nrow=dim(Yrest)[1],ncol=2)
	for (n in c(1:dim(Yrest)[1])) {
		NullChan[n,1] = all(diff(Yrest[n,])==0)
		NullChan[n,2] = all(diff(Ytask[n,])==0)
	}
	NullChan = any(apply(NullChan==1,2,any))

	if (!NullChan) {
		Yrest = t(apply(Yrest,1,normalize))
		Ytask = t(apply(Ytask,1,normalize))
		Y = cbind(Yrest,Ytask)

		Tg = c(dim(Yrest)[2],dim(Ytask)[2])
		Tall = dim(Y)[2]
		G = 2
		group = matrix(0,nrow=Tall,ncol=G)
		group[c(1:Tg[1]), 1] = 1
		for (g in c(2:G)) {
			group[c((1+sum(Tg[c(1:(g-1))])):sum(Tg[c(1:g)])),g] = 1
		}

		rm(Yrest)
		rm(Ytask)

		dim(Y) = c(dim(Y),1)
		N = dim(Y)[1]
		Tall = dim(Y)[2]
		S = dim(Y)[3]
		G = dim(group)[2]

		if (is.null(d_sigma)) {
			d_sigma = 1/mean(apply(Y,c(1,3),var))*(c_sigma-1)
		}
		
		mcmc = gibbs_dfsv_SS(Y,group,K=Kest,
			nburnin=nburnin,niter=niter,nthin=nthin, 
			B011inv=0.01,B022inv=0.01,
			MHsteps=2, MHextra=FALSE,
			Bsigma=Bsigma, m_piIn=m_pi,
			c_sigma=c_sigma,d_sigma=d_sigma,
			dontupdatemu=FALSE,
			MultiSubject=TRUE,
			NormVarLambdaNK=2,
			EstimateObsMu=FALSE,
			IsotropicObsVar=TRUE,
			fixdiag=TRUE,
			debug=FALSE,
			Gammaprior=TRUE,
			FactorLevel=TRUE)

		fname = paste0("mcmc_single_sens_",sid,"_K",Kest,"_mpi", m_pi[1],"_csigma,",c_sigma,"_dsigma",d_sigma,"_aphi",a_phi,"_Bsigma",Bsigma,"_Bmu",B_mu,".rds")
		saveRDS(mcmc,file=file.path(save_path,fname))
	}

	
}

