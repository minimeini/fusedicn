#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=40
#SBATCH --partition=batch
#SBATCH -J fusedicn
#SBATCH -o fusedicn.%J.out
#SBATCH -e fusedicn.%J.err
#SBATCH --mail-user=meini.tang@kaust.edu.sa
#SBATCH --mail-type=ALL
#SBATCH --time=34:00:00
#SBATCH --mem-per-cpu=8G

## run the application:
isibex=${1:-1}
nburnin=${2:-10000}
niter=${3:-10000}
nsub=${4:-200}
kest=${5:-18}
cont=${6:-0}
prevjobid=${7:-00000000}
mcmcfilename=${8:-"mcmc_nsub_200_K18_20Nov13.rds"}

if [[ $isibex -eq 1 ]]
then
	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
	module load gcc/6.4.0
	module load R/3.6.0/gnu-6.4.0
	rest_path="/ibex/scratch/tangm0a/data/rfMRI_roi_mean/AAL90/REST1"
	task_path="/ibex/scratch/tangm0a/data/tfMRI_roi_mean/AAL90/LANGUAGE"
	behav_path="/ibex/scratch/tangm0a/data/behav/hcp_language_score.csv"
else
	rest_path="/home/tangm0a/data/rfMRI_roi_mean/AAL90/REST1"
	task_path="/home/tangm0a/data/tfMRI_roi_mean/AAL90/LANGUAGE"
	behav_path="/home/tangm0a/data/behav/hcp_language_score.csv"
fi


Rscript ../../bicnet/rutils/inst.R

if [[ $cont -eq 0 ]]
then
	R -e "source('main_fused_group.R');main_fused_group(rest_path='${rest_path}',task_path='${task_path}',behav_path='${behav_path}',nburnin=${nburnin},niter=${niter},nsub_max=${nsub},Kest=${kest})"
else
	R -e "source('main_fused_group.R');cont_fused_group(nburnin=${nburnin},niter=${niter},mcmc_filename='${mcmcfilename}',prev_jobid='${prevjobid}',rest_path='${rest_path}',task_path='${task_path}',behav_path='${behav_path}')"
fi

if [[ $isibex -eq 1 ]]
then
	rsync -a /tmp/output tangm0a@ilogin.ibex.kaust.edu.sa:/ibex/scratch/tangm0a/repository/fusedicn/output/${SLURM_JOB_ID}
fi
echo "Done."
