unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     pfix="/ibex/scratch/tangm0a";;
    Darwin*)    pfix="/Users/meini";;
    *)          pfix="/UNKNOWN"
esac

rest_path="${pfix}/data/rfMRI_roi_mean/AAL90/REST1"
task_path="${pfix}/data/tfMRI_roi_mean/AAL90/LANGUAGE"
rest_file=($(ls ${rest_path}))
task_file=($(ls ${task_path}))

sidr=()
for fr in "${rest_file[@]}"; do
	tmpr=$(echo ${fr} | awk -F'_' '{ print $6 }');
	tmpr=$(echo ${tmpr} | grep -o -E '[0-9]+');
	sidr+=("${tmpr}");
done

sidt=()
for ft in "${rest_file[@]}"; do
	tmpt=$(echo ${ft} | awk -F'_' '{ print $6 }');
	tmpt=$(echo ${tmpt} | grep -o -E '[0-9]+');
	sidt+=("${tmpt}");
done

sid=()
for sr in "${sidr[@]}"; do
    for st in "${sidt[@]}"; do
        if [[ ${sr} = ${st} ]]; then
            sid+=("${sr}");
        fi
    done
done

kest=(12 14 16 18 20 22 24)
for ss in ${sid[@]}; do
    for k in ${kest[@]}; do
	   sbatch ./run_single.sh ${ss} ${k}
    done
done
