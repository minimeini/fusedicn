#!/bin/bash
mpi=(0.9 0.7 0.5) #3
csigma=(1 0.1 0.001) #3
dsigma=(1 0.1 0.001) #3
aphi=(20 10 2.5) #3
Bsigma=(0.5 1 10) #3
Bmu=(1 10) #2
# 486 in total

for mp in ${mpi[@]}; do
    for cs in ${csigma[@]}; do
    	for ds in ${dsigma[@]}; do
    		for ap in ${aphi[@]}; do
    			for Bs in ${Bsigma[@]}; do
    				for Bm in ${Bmu[@]}; do
	   					sbatch ./run_sensitivity_sub.sh ${mp} ${cs} ${ds} ${ap} ${Bs} ${Bm}
	   				done
	   			done
	   		done
	   	done
    done
done
