#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=40
#SBATCH --partition=batch
#SBATCH -J singleicn
#SBATCH -o singleicn.%J.out
#SBATCH -e singleicn.%J.err
#SBATCH --mail-user=meini.tang@kaust.edu.sa
#SBATCH --mail-type=ALL
#SBATCH --time=01:00:00
#SBATCH --mem-per-cpu=8G

## run the application:
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
module load gcc/6.4.0
module load R/3.6.0/gnu-6.4.0
rest_path="/ibex/scratch/tangm0a/data/rfMRI_roi_mean/AAL90/REST1"
task_path="/ibex/scratch/tangm0a/data/tfMRI_roi_mean/AAL90/LANGUAGE"
R -e "source('main_sensitivity_single.R');main_sensitivity_single(sid='100206',Kest=18,rest_path='${rest_path}',task_path='${task_path}',nburnin=10000,niter=20000,m_pi=$1,c_sigma=$2,d_sigma=$3,a_phi=$4,Bsigma=$5,B_mu=$6);q();"

echo "Done $1."
