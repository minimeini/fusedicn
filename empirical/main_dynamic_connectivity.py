'''
Calculate time-varying connectivity.

1. Time-varying correlation:
	- sliding window + pearson's sample correlation + z-transform
	- default value for diagonal is ztransform(1)=+Inf.
'''



# for ipython
# %load_ext autoreload
# %autoreload 2
import sys,os,math,psutil,time
import numpy as np
from scipy import stats
from timeit import default_timer as timer
import itertools


import pathos.pools as pools
# from pathos.threading import ThreadPool
# only count the physical cpus
num_cpus = psutil.cpu_count(logical=True)
thrd_pool = pools.ThreadPool(nodes=num_cpus)
proc_pool = pools.ProcessPool(nodes=num_cpus)


def get_gitpath(debug=False):
	if debug:
		gitpath = os.getenv("HOME") + '/respository/base-connectivity'
	else:
		curpath = os.path.abspath(os.path.dirname(sys.argv[0]))
		gitpath = ('/').join(curpath.split(sep='/')[:-1])
	return gitpath


gitpath = get_gitpath(debug=False)
sys.path.insert(1,gitpath+'/utils')
import utils


class network(object):
	def __init__(self, sublist, loadpath, savepath, savefile, width=15, 
		stepsize=1, method="pearsonr", binary=False, 
		ztransformed=True, diagval=0):
		
		self.width = width
		self.stepsize = stepsize

		self.method = method
		self.binary = binary
		self.ztransformed = ztransformed
		self.diagval = diagval

		self.num_cpus = psutil.cpu_count(logical=True)
		self.proc_pool = pools.ProcessPool(nodes=self.num_cpus)
		self.thrd_pool = pools.ThreadPool(nodes=self.num_cpus)

		self.loadpath = loadpath
		self.loadfrmt = ".csv"

		self.savepath = savepath
		self.savefile = savefile
		self.savefrmt = '.npy'
		return


	def read_data(self, sid):
		assert(type(sid)==str or type(sid)==int or type(sid)==float)
		path = self.loadpath + str(sid) + self.loadfrmt
		data = np.genfromtxt(path, delimiter=',').squeeze()
		return data


	def get_conn_paths(self, sid):
		savepath = self.savepath+'/'+self.savefile+str(sid)+self.savefrmt
		return savepath


	def __calc_conn_seq(self,it,data):
		i = it[0]
		j = it[1]
		x = data[i,:].squeeze()
		y = data[j,:].squeeze()

		if self.method == 'pearsonr': 
			# pearson's sample correlation
			c = stats.pearsonr(x,y)[0]
		elif self.method == 'spearmanr':
			c = stats.spearmanr(x,y)[0]
		elif self.method == 'kendalltau':
			c = stats.kendalltau(x,y)[0]

		if self.ztransformed:
			c = np.arctanh(c)

		return i,j,c


	def calc_conn(self,data):
		'''
		Calculate connectivity.


		>>> REQUIRED INPUT

		- data: numpy 2darray, nroi x ntime


		>>> OPTIONAL INPUT

		- method
		- binary
		- ztransformed
		- diagval


		>>> OUTPUT

		- conn: numpy 2darray, nroi x nroi

		'''
		nroi = data.shape[0]
		iters = itertools.combinations(range(nroi),2)

		for x in data:
			try:
				assert(np.sum( np.abs(x) ) > 0)
			except AssertionError:
				# print("All entries are zero.")
				raise AssertionError

			try:
				assert(np.sum( np.square(x - np.mean(x)) ) > 0)
			except AssertionError:
				# print("Zero denominator")
				raise AssertionError

		results = []
		for it in iters:
			results.append(self.__calc_conn_seq(it,data))

		rows = [r[0] for r in results]
		cols = [r[1] for r in results]
		vals = [r[2] for r in results]
		
		conn = np.zeros((nroi, nroi))
		if not self.ztransformed:
			np.fill_diagonal(conn, self.diagval)
		else:
			np.fill_diagonal(conn, np.arctanh(self.diagval))
		
		conn[rows,cols] = vals
		conn[cols,rows] = vals

		return conn


	def sliding_window(self,ts_data):
		'''
		Returns a sliding window (of width n) over data from the iterable.
			s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...


		>>> REQUIRED INPUT

		- ts_data: numpy 2darray, nroi x ntime.
		- wlen: int
		- shift: int

		>>> OUTPUT

		- win_data: list of numpy 2darray (nroi x width)


		>>> Performance Notes

		- for loop faster than list comprehension faster than map

		'''
		assert(self.width>0)
		nroi = ts_data.shape[0]
		npad = math.floor(self.width/2)

		pad_data = np.hstack((np.zeros((nroi,npad)),ts_data,np.zeros((nroi,npad))))
		nt = pad_data.shape[1]

		start = np.arange(0, nt-self.width+1, self.stepsize)
		nwin = start.shape[0]

		win_data = []
		for nw in range(nwin):
			win_data.append(pad_data[:,start[nw]:start[nw]+self.width])

		return win_data


	def calc_win_conn(self,sid,parallel=True,save=True):
		'''
		Calculate time-varying connectivity using sliding window method.


		>>> REQUIRED INPUT

		- ts_data: numpy 2darray, nroi x ntime.


		>>> OUTPUT

		- conn: numpy 3darray, nroi x nroi x nwin
			(
			 NOTES: The order of windows is consistent with the order of 
			 windowed data returned by `sliding_window`, which also means
		 	 nits order is consistent with the order of real time.
		 	)

		'''
		ts_data = self.read_data(str(sid))
		win_data = self.sliding_window(ts_data)
		assert(type(win_data)==list)

		try:
			if parallel:
				conn = self.proc_pool.amap(self.calc_conn, win_data)
				while not conn.ready():
					time.sleep(5)
				conn = conn.get()
			else:
				conn = []
				for data in win_data:
					conn.append(self.calc_conn(data))

		except AssertionError:
			msg = "Something is wrong with subject {}".format(sid)
			print(msg)
			return 1 # skip this subject and return error value

		assert(type(conn)==list)
		conn = np.concatenate([mat[:,:,np.newaxis] for mat in conn],axis=2)

		if save:
			if sid is not None:
				savepath = self.get_conn_paths(sid)
				np.save(savepath, conn)
				return 0
			else:
				return 1
		else:
			return conn


def main(data_type, roi_name="AAL90", labels=None, nsub=None):
	if(type(labels)==str):
		tmp = labels
		labels = []
		labels.append(tmp)

	print(">>> START")
	print("Dataset: {}".format(data_type))

	for task in labels:
		print("Task: {}".format(task))
		
		info = utils.TaskLoader(data_type=data_type, task=task, 
			roi_name=roi_name)
		sublist = info.get_filelist(info.roi_paths.spar)
		sublist = info.get_subid(sublist, sorting=True)

		nsub = min(len(sublist), nsub)
		sublist = sublist[:nsub]
		print("Number of subject: {}".format(len(sublist)))


		loadpath = info.paths.roi_proc
		savepath = info.pfix()+'/data/'+data_type+'_connmat/'+roi_name+'/'+task
		savefile = data_type+'_'+task+'_connmat_'
		if not os.path.exists(savepath):
			os.makedirs(savepath)

		nw = network(sublist, loadpath, savepath, savefile)

		start = timer()
		for sid in sublist:
			nw.calc_win_conn(sid)
		end = timer()

		print("Elapsed time: {} sec".format(end-start))

	print(">>> END")
	return


if __name__ == "__main__":
	print()
	print("--- Initialization Start ---")
	print(">>> Provide data type - either 'tfMRI' or 'rfMRI': ")
	data_type = str(input())

	print(">>> Provide task type - multitask use single space as a delimiter: ")
	labels = str(input())
	labels = labels.upper().split(sep=' ')

	print(">>> Specify the number of subjects - set to 99999 for all subjects")
	nsub = int(input())
	
	print("--- Initialization Done ---")
	print()
	main(data_type, labels=labels, roi_name="AAL90", nsub=nsub)

	
