import os
import numpy as np
import empirical_utils as emp
import matplotlib.pyplot as plt


def load_data(wd, nload=None, detrend=True):
	'''
	'''
	filelist = [f for f in os.listdir(wd) if 'csv' in f]
	data = []
	if nload is None:
		nload = len(filelist)
	else:
		nload = min(len(filelist), nload)

	for i in range(nload):
		fpath = wd+'/'+filelist[i]
		data.append(np.genfromtxt(fpath, delimiter=',').T)

	data = np.asarray(data).squeeze() # nsub, ntime, nroi
	data = emp.detrend(data)
	return data


def plot_stovol(stovol, ords_sv=None):
	plt.ioff()
	colors = ['blue', 'orange', 'green', 'red', 'purple']
	fig, ax = plt.subplots(2,1,figsize=(9,6))
	for i in range(len(stovol)):
		msg = "F{}".format(i+1)
		if ords_sv is not None:
			msg = msg + ": I({:1.0f})".format(ords_sv[i])

		ax[0].plot(stovol[i], label=msg)
		ax[1].acorr(stovol[i].flatten()-np.mean(stovol[i].flatten()), 
			maxlags=None, color=colors[i], label=msg)

	ax[0].legend(loc='upper left', fontsize='x-small')
	ax[0].set_title("Trace Plot")
	ax[1].legend(loc='upper left', fontsize='x-small')
	ax[1].set_title("Autocorrelation")
	fig.tight_layout()
	plt.ion()
	return fig,ax


def main(exp_type="rfMRI", exp_name="REST1", nload=None, nfactor=5):
	dpath = "/Users/meini/data/"
	wd = dpath + exp_type + "_roi_mean/" + exp_name
	savepath = dpath + "visualization/stovol"
	os.makedirs(savepath, exist_ok=True)

	print(">>> Load and process data.")
	data = load_data(wd, nload=nload, detrend=True)

	print(">>> Run PCA on data.")
	f, U, err = emp.pca_everything(data, nfactor=nfactor)

	print(">>> Logarithm of Factor Variances.")
	nsub = data.shape[0]
	for s in range(nsub):
		# win_data: a list (ntime) of nfactor x winlen
		win_data = emp.sliding_window(f[s,:,:].squeeze().T)
		stovol = emp.calc_conn(win_data)[:,:,11:-10] # nfactor x nfactor x ntime
		stovol = [np.log(np.abs(stovol[i,i,:])).squeeze() for i in range(nfactor)]
		# stovol: a list (nfactor) of (ntime) logarithm of factor variances
		ords_sv = np.asarray(emp.find_integration_order(stovol))

		fig, ax = plot_stovol(stovol, ords_sv=ords_sv)
		fig.savefig(savepath+"/"+exp_type+"_"+exp_name+"_sub{}.png".format(s+1), 
			dpi=300)
		plt.close("all")

	return


