# for ipython
# Type `%load_ext` to enable autoreload
# Type `%autoreload 2` to auto reload all packages everytime you run a command

import sys,os,math,psutil,time
import numpy as np
from scipy import stats
from timeit import default_timer as timer
import itertools

# Parallel computing
import pathos.pools as pools
# from pathos.threading import ThreadPool
# only count the physical cpus
num_cpus = psutil.cpu_count(logical=True)
proc_pool = pools.ProcessPool(nodes=num_cpus)


def get_gitpath(debug=False):
	if debug:
		gitpath = os.getenv("HOME") + '/respository/base-connectivity'
	else:
		curpath = os.path.abspath(os.path.dirname(sys.argv[0]))
		gitpath = ('/').join(curpath.split(sep='/')[:-1])
	return gitpath


gitpath = get_gitpath(debug=False)
sys.path.insert(1,gitpath+'/utils')
import utils


class ConnTest(object):
	def __init__(self, rest_label, task_label, roi_name="AAL90", test="ttest"):
		self.rest_label = rest_label
		self.task_label = task_label
		self.roi_name = roi_name
		self.test = test

		self.pfix = os.getenv("HOME")
		self.loadpath_rest = self.pfix+'/data/rfMRI_connmat/'+roi_name+'/'+rest_label
		self.loadfile_rest = 'rfMRI_'+rest_label+'_connmat_'
		self.loadpath_task = self.pfix+'/data/tfMRI_connmat/'+roi_name+'/'+task_label
		self.loadfile_task = 'tfMRI_'+task_label+'_connmat_'
		self.loadfmrt = '.npy'

		self.savepath = self.pfix+'/data/int_test/'+roi_name
		if not os.path.exists(self.savepath):
			os.makedirs(self.savepath)
			return

		self.savefile = test+'_'+rest_label+'_'+task_label+'.npy'

		self.sid = None
		self.data_rest = None
		self.data_task = None

		num_cpus = psutil.cpu_count(logical=True)
		self.proc_pool = pools.ProcessPool(nodes=num_cpus)
		return


	def get_sid(self, excld=['Store','tar']):
		flist = os.listdir(self.loadpath_rest)
		for pattern in excld:
			flist = [f for f in flist if pattern not in f]

		flist = np.sort(np.asarray(flist)).tolist()
		sid_rest = [f.split(sep='_')[-1].split(sep='.')[0] for f in flist]


		flist = os.listdir(self.loadpath_task)
		for pattern in excld:
			flist = [f for f in flist if pattern not in f]

		flist = np.sort(np.asarray(flist)).tolist()
		sid_task = [f.split(sep='_')[-1].split(sep='.')[0] for f in flist]

		sid = list(set(sid_rest) & set(sid_task))
		assert(len(sid)>0)
		return sid
		

	def load_data(self, sid):
		self.sid = sid
		loadpath_rest = self.loadpath_rest+'/'+self.loadfile_rest+str(sid)+self.loadfmrt
		loadpath_task = self.loadpath_task+'/'+self.loadfile_task+str(sid)+self.loadfmrt
		self.data_rest = np.load(loadpath_rest)
		self.data_task = np.load(loadpath_task)
		assert(self.data_rest.shape[0]==self.data_task.shape[0])
		return


	def int_ttest(self, it):
		'''
		>>> MISC

		scipy.stats.ttest_ind is a two-sided t-test
		'''
		i = it[0]
		j = it[1]
		T, pval = stats.ttest_ind(self.data_rest[i,j,:].squeeze(),
			self.data_task[i,j,:].squeeze(), equal_var=False)
		G = T
		return i,j,G


	def int_kstest(self, it):
		'''
		>>> MISC

		scipy.stats.ttest_ind is a two-sided t-test
		'''
		i = it[0]
		j = it[1]
		D, pval = stats.ks_2samp(self.data_rest[i,j,:].squeeze(), 
			self.data_task[i,j,:].squeeze())
		G = D
		return i,j,G


	def pairwise_ind_test(self):
		nroi = self.data_rest.shape[0]
		if nroi <= 1:
			nroi = 90

		pconn = np.zeros((nroi, nroi))

		try:
			assert(np.sum(np.abs(self.data_rest)>0))
			assert(np.sum(np.abs(self.data_task)>0))
		except AssertionError:
			print("AssertionError - Empty Input.", end=" ")
			return pconn

		iters = itertools.combinations(range(nroi),2)

		if self.test == "ttest":
			results = self.proc_pool.amap(self.int_ttest, iters)
		elif self.test == "kstest":
			results = self.proc_pool.amap(self.int_kstest, iters)

		while not results.ready():
			time.sleep(3)
		results = results.get()
		assert(type(results)==list)
		
		for r in results:
			pconn[r[0],r[1]] = r[2]
			pconn[r[1],r[0]] = r[2]

		try: 
			assert(np.sum(np.abs(pconn))>0)
		except AssertionError: 
			print("AssertionError - Empty Output.", end=" ")

		try: 
			assert(np.allclose(pconn, pconn.T))
		except AssertionError:
			print("AssertionError - Nonsymmetric.", end=" ")

		return pconn



def main(rest_label, task_label, roi_name="AAL90"):
	print(">>> START")

	actor = ConnTest(rest_label, task_label, roi_name=roi_name, test="ttest")
	sublist = actor.get_sid()
	nsub = len(sublist)
	pconn = np.zeros((90,90,nsub))

	print("Test: {}".format(actor.test))
	print("{} subjects in total.".format(nsub))
	print()

	for i,sid in zip(range(nsub), sublist):
		print("Subject {}:".format(sid), end=" ")
		start = timer()
		try:
			actor.load_data(sid)
			pconn[:,:,i] = actor.pairwise_ind_test()
		except:
			print("Unexpected error.", end=" ")
			pass

		end = timer()
		print("{} sec".format(end-start))

	np.save(actor.savepath+'/'+actor.savefile, pconn)

	print()

	actor = ConnTest(rest_label, task_label, roi_name=roi_name, test="kstest")
	sublist = actor.get_sid()
	nsub = len(sublist)
	pconn = np.zeros((90,90,nsub))

	print("Test: {}".format(actor.test))
	print("{} subjects in total.".format(nsub))
	print()

	for i,sid in zip(range(nsub), sublist):
		print("Subject {}:".format(sid), end=" ")
		start = timer()
		try:
			actor.load_data(sid)
			pconn[:,:,i] = actor.pairwise_ind_test()
		except:
			print("Unexpected error.", end=" ")
			pass

		end = timer()
		print("{} sec".format(end-start))


	np.save(actor.savepath+'/'+actor.savefile, pconn)

	print(">>> END")
	return 0


if __name__ == "__main__":
	print()
	print("--- Initialization Start ---")
	print(">>> Select one task type for rfMRI:")
	rest_label = str(input()).upper()

	print(">>> Select one task type for tfMRI:")
	task_label = str(input()).upper()
	
	print("--- Initialization Done ---")
	print()
	main(rest_label, task_label, roi_name="AAL90")

