# Type `%load_ext autoreload` in iPython to enable autoreload

import os, sys, gc
import numpy as np
import matplotlib.pyplot as plt


def get_gitpath(debug=False):
	if debug:
		gitpath = os.getenv("HOME") + '/respository/base-connectivity'
	else:
		curpath = os.path.abspath(os.path.dirname(sys.argv[0]))
		gitpath = ('/').join(curpath.split(sep='/')[:-1])
	return gitpath


gitpath = get_gitpath(debug=False)
sys.path.insert(1,gitpath+'/utils')
import vis_utils as vu


def main(roi_name="AAL90", test_type="ttest", rest_type="REST1", task_type="LANGUAGE", save=False, show=True):
	datapath = os.getenv("HOME") + '/data'
	savepath = datapath + '/visualization'
	filepath = datapath+"/int_test/"+roi_name+"/"+test_type+"_"+rest_type+"_"+task_type+".npy"

	if test_type=="ttest":
		label = "t-test"
	elif test_type=="kstest":
		label = "KS-test"

	title = "{}: {} vs {}".format(label, rest_type.lower(), task_type.lower())

	wcorr = np.load(filepath)
	roipath = datapath + '/roi_info/AAL90/AAL90_MNI_2mm'
	coords, labels = vu.get_coords(roipath)


	fig, ax = vu.plot_matrices_brainnet(np.abs(wcorr), coords, ncol=4, 
		thres=0.98, height=3, width=1.3, fontsize=6, 
		node_size=0.3, plot_aver=True, linewidth=0.8)
	fig.suptitle(title, fontsize=6, x=0.51,y=0.1)

	if show:
		plt.show(fig)

	if save:
		fig.savefig(savepath+'/brainnet_'+test_type+'_'+task_type.lower()+'.png', 
			dpi=300, pad_inches=0.05, format='png', bbox_inches='tight')
		plt.close(fig)
		gc.collect()


	fig, ax = vu.plot_matrices_heatmap(wcorr, ncol=4, height=3, width=1.5, 
		labels=None, plot_aver=True, normalize=True, absolute=True)
	fig.suptitle(title, fontsize=6, y=0.9)

	if show:
		plt.show(fig)

	if save:
		fig.savefig(savepath+'/heatmap_'+test_type+'_'+task_type.lower()+'.png', 
			dpi=300, pad_inches=0.05, format='png', bbox_inches='tight')
		plt.close(fig)
		gc.collect()

	return

