args = commandArgs(trailingOnly = TRUE)

nfac = 20
if (length(args) >= 1) {
    nfac = as.integer(args[1])
}

stat = 'median'
if (length(args) >= 2) {
    stat = as.character(args[2])
}

library(factorstochvol)
get_stat <- function(y, Lambda, fac, logvar) {
    ntime <- dim(y)[1]

    var <- exp(logvar)
    prec <- exp(-logvar)

    yhat <- t(Lambda %*% fac)
    yres <- abs(y - yhat)

    res <- apply(yres, 2, function(x) {
        sum(x^2)
    })

    mae = mean(c(yres))
    rmse = sqrt(mean(c(yres)^2))
    loglik <- -0.5 * (sum(res * prec) + sum(logvar) * ntime)
    return(c(mae, rmse, loglik))
}


dpath = file.path(path.expand('~'), 'data')
info = read.table(file.path(dpath, 'bicnet', 'info', 'hcp_subject_info.txt'))
sid_list = info$V1[info$V2 == 1]
task = c('REST1', 'EMOTION', 'GAMBLING', 'LANGUAGE', 'MOTOR', 'RELATIONAL', 'SOCIAL', 'WM')
nroi = 90

seed = sample.int(9999, 1)
log_file = file.path(dpath, 'bicnet', paste0(paste('mcmc', nfac, stat, seed, sep = '_'), '.txt'))
stat_file = file.path(dpath, 'mcmc_fsv', paste0('mcmc', nfac, '.txt'))
stat_sample_file = file.path(dpath, 'mcmc_fsv', paste0('mcmc', nfac, '_sample.txt'))

sink(file = log_file)
print(paste0("Log file: ", log_file))
print(paste0("Statistics will be written to ", stat_file, ' and ', stat_sample_file))

if (!file.exists(stat_file)) {
    write(paste('sid', 'cnt', 'nfac', 'mae', 'rmse', 'loglik', collapse = ' '), sep = '\n', file = stat_file)
}

if (!file.exists(stat_sample_file)) {
    write(paste('sid', 'cnt', 'nfac', 'mae', 'rmse', 'loglik', collapse = ' '), sep = '\n', file = stat_sample_file)
}

for (sid in sid_list[-c(1:50)]) {
    y = NULL
    cnt = 0
    for (tt in task) {
        if (tt == "REST1") {
            modality <- "rfMRI"
        } else {
            modality <- "tfMRI"
        }

        fname <- paste0(paste(modality, tt, "LR", sid, stat, sep = "_"), ".txt")
        fname <- file.path(dpath, modality, tt, fname)
        if (file.exists(fname)) {
            dat <- t(as.matrix(read.table(fname)))
            if (dim(dat)[2] == nroi) {
                cnt = cnt + 1
                y <- rbind(y, dat)
            }
        }
    }

    print(paste0("Subject ", sid, " has ", cnt, " datasets."))

    if (!is.null(y)) {
        print(paste0("Start MCMC for subject ", sid))
        tryCatch({
            out <- fsvsample(
                y,
                factors = nfac, restrict = "upper", zeromean = TRUE,
                draws = 2000, burnin = 2000, thin = 2, quiet = TRUE
            )

            niter = 1000
            ntime <- dim(y)[1]
            nroi <- dim(y)[2]

            Lambda <- apply(out$facload, c(1, 2), mean)
            fac <- out$runningstore$fac[, , 1]
            logvar_hat <- apply(out$logvar0[1:90, ], 1, mean)

            stat_med = NULL
            for (i in 1:niter) {
                tmp = get_stat(y, out$facload[, , i], fac, out$logvar[1, 1:90, i])
                if (is.null(stat_med)) {
                    stat_med = tmp
                } else {
                    stat_med = rbind(stat_med, tmp)
                }
            }
            stat_med = apply(stat_med, 2, median)
            out_stats_med <- c(sid, cnt, nfac, stat_med)
            write(paste(out_stats_med, collapse = " "), file = stat_sample_file, append = TRUE, sep = "\n")
            print(paste0("Done mean: mae = ", stat_med[1], ", rmse = ", stat_med[2], ", and loglik = ", stat_med[3]))

            stat_hat = get_stat(y, Lambda, fac, logvar_hat)
            out_stats <- c(sid, cnt, nfac, stat_hat)
            write(paste(out_stats, collapse = " "), file = stat_file, append = TRUE, sep = "\n")
            print(paste0("Done hat: mae = ", stat_hat[1], ", rmse = ", stat_hat[2], ", and loglik = ", stat_hat[3]))

            # var_list <- c("out", "out_stats")
            # fname <- paste0(paste("mcmc", nfac, sid, stat, sep = "_"), ".rdata")
            # fname <- file.path(dpath, "mcmc_fsv", fname)
            # save(list = var_list, file = fname)
            # print(paste0("Save output as ", fname))
        }, error = function(e){print("MCMC failed")})
    } else {
        print(paste0("Skip because not data for subject ", sid))
    }
    print("\n")
}

sink(file = NULL)