library(foreach)
library(doSNOW)

cores = parallel::detectCores()
cl = makeCluster(cores - 1)
registerDoSNOW(cl)
foreach::getDoParRegistered()

dpath = file.path(path.expand('~'), 'data')
info = read.table(file.path(dpath, 'bicnet', 'info', 'hcp_subject_info.txt'))
sid_list = info$V1[info$V2 == 1]

pb <- txtProgressBar(max = length(sid_list), style = 3)
progress <- function(n) setTxtProgressBar(pb, n)
opts <- list(progress = progress)


foreach(sid = sid_list, .options.snow = opts) %dopar% {
    source('./fsv_settings.r')

    y = NULL
    cnt = 0
    for (tt in task) {
        if (tt == "REST1") {
            modality <- "rfMRI"
        } else {
            modality <- "tfMRI"
        }

        fname <- paste0(paste(modality, tt, "LR", sid, stat, sep = "_"), ".txt")
        fname <- file.path(dpath, modality, tt, fname)
        if (file.exists(fname)) {
            dat <- t(as.matrix(read.table(fname)))
            if (dim(dat)[2] == nroi) {
                cnt = cnt + 1
                y <- rbind(y, dat)
            }
        }
    }


    if (!is.null(y)) {
        tryCatch({
            out <- fsvsample(
                y,
                factors = nfac, restrict = "upper", zeromean = TRUE,
                draws = 2000, burnin = 2000, thin = 2, quiet = TRUE
            )

            niter = 1000
            ntime <- dim(y)[1]
            nroi <- dim(y)[2]

            Lambda <- apply(out$facload, c(1, 2), mean)
            fac <- out$runningstore$fac[, , 1]
            logvar_hat <- apply(out$logvar0[1:90, ], 1, mean)

            stat_hat = get_stat(y, Lambda, fac, logvar_hat)
            out_stats <- c(sid, cnt, nfac, stat_hat)

            Lambda = out$facload # N x K x nsample
            var_list <- c("Lambda", "out_stats", "y")
            fname <- paste0(paste("mcmc", nfac, sid, stat, sep = "_"), ".rdata")
            fname <- file.path(dpath, "mcmc_fsv_facload", fname)
            save(list = var_list, file = fname)

            rm(out)
            return(1)

        }, error = function(e){return(0)})
    } else {
        return(0)
    }
}


stopCluster(cl = cl)



# Calculate group inclusion probability

grp_pi = NULL
thres = 1e-2
source('./fsv_settings.r')
nsub = 0
for (sid in sid_list) {
    fname = paste("mcmc", nfac, sid, stat, sep = "_")
    fname = file.path(dpath, "mcmc_fsv_facload", paste0(fname, ".rdata"))
    if (file.exists(fname)) {
        tmp = new.env()
        load(fname, envir = tmp)
        Zmap = array(0, dim = dim(tmp$Lambda))
        if (is.null(grp_pi)) {
            grp_pi = Zmap
        }

        Zmap[abs(tmp$Lambda) > thres] = 1
        grp_pi = grp_pi + Zmap
        nsub = nsub + 1
    }
}

grp_pi = grp_pi / nsub

Zmap = apply(grp_pi, c(1, 2), median)
Zmap[Zmap > 0.8] = 1
Zmap[Zmap <= 0.8] = 0

fname = paste("mcmc", nfac, stat, "group", sep = "_")
fname = file.path(dpath, "bicnet", paste0(fname, ".rdata"))
var_list = c("grp_pi", "thres", "Zmap")
save(list = var_list, file = fname)
