library(factorstochvol)
get_stat <- function(y, Lambda, fac, logvar) {
    ntime <- dim(y)[1]

    var <- exp(logvar)
    prec <- exp(-logvar)

    yhat <- t(Lambda %*% fac)
    yres <- abs(y - yhat)

    res <- apply(yres, 2, function(x) {
        sum(x^2)
    })

    mae = mean(c(yres))
    rmse = sqrt(mean(c(yres)^2))
    loglik <- -0.5 * (sum(res * prec) + sum(logvar) * ntime)
    return(c(mae, rmse, loglik))
}


nfac = 20
stat = 'median'
task = c('REST1', 'EMOTION', 'GAMBLING', 'LANGUAGE', 'MOTOR', 'RELATIONAL', 'SOCIAL', 'WM')
nroi = 90
dpath = file.path(path.expand('~'), 'data')