#!/bin/bash


phase="LR"
nfactor=20

hcp_id="welkin31"
hcp_pswd="hetcube31"

output="timeseries"

while getopts ":s:t:p:u:r:d:f:o:" opt; do
  case $opt in
    s)
    sid=$OPTARG
    ;;
    t)
    task=$OPTARG
    ;;
    p)
    phase=$OPTARG
    ;;
    r)
    repo=$OPTARG
    ;;
    d)
    datapath=$OPTARG
    ;;
    f)
    nfactor=$OPTARG
    ;;
    o)
    output=$OPTARG # timeseries, mean, median, mode
    ;;
    *)
    echo "Invalid argument."
    ;;
  esac
done

if [[ "${task}" =~ "REST" ]]; then 
    modality="rfMRI"
else
    modality="tfMRI"
fi


pname="${datapath}/${modality}/${task}"

fname="${modality}_${task}_${phase}_${sid}_${output}.txt"
fsize=$( ${repo}/preprocessing/get_file_size.sh "${pname}/${fname}" )

fname0="${spath}/${label}_${output}_${sid}.txt"
fsize0=$( ${repo}/preprocessing/get_file_size.sh "${pname}/${fname0}" )

if [ -f "${pname}/${fname}" ] && [ ${fsize} -gt 1 ]; then 
    echo "Data file already exists."
elif [ -f "${pname}/${fname0}" ] && [ ${fsize0} -gt 1 ]; then
    echo "Data file exists but need to be renamed."
    mv "${pname}/${fname0}" "${pname}/${fname}"
else
    3dinfo > /dev/null 2>&1
    if [ $? -eq 0 ] || [ ${output} == "timeseries" ]; then
        echo "Try to create data file ${pname}/${fname}."
        start=`date +%s`
        bash "${repo}/preprocessing/get_data.sh" -s ${sid} -t "${task}" -p "${phase}" -u "${hcp_id}:${hcp_pswd}" -r "${repo}" -d "${datapath}" -o "${output}"
        if [ $? -eq 0 ]; then
            end=`date +%s`
            runtime=$((end-start))
            echo "Elapsed time: ${runtime}"
        else
            echo "Fail to get the input data"
            exit 1
        fi
    else
        echo "Skip: cannot run AFNI."
        exit 1
    fi
fi

# if [ ! -f "${pname}/${fname}" ] &&  ; then
#     if [[ "${USER}" == "mtang41" ]]; then
#         echo "Skip: ${pname}/${fname} doesn't exist"
#         exit 0
#     else
#         echo "Try to create file ${pname}/${fname}."
        
#         bash "${repo}/preprocessing/get_data.sh" -s ${sid} -t "${task}" -p "${phase}" -u "${hcp_id}:${hcp_pswd}" -r "${repo}" -d "${datapath}" -o "${output}"
#         end=`date +%s`
#         runtime=$((end-start))
#         RESULT=$?
#         if [ $RESULT -eq 0 ]; then
#             echo "Elapsed time: ${runtime}"
#         else
#             echo "Fail to get the input data"
#             exit 1
#         fi
#     fi
# fi

pname2="${datapath}/mcmc_fsv"
fname2="mcmc_f${nfactor}_${sid}_${task}_${output}.rdata"
if [ -f "${pname2}/${fname2}" ]; then
    echo "File ${pname2}/${fname2} already exists - will be overwritten."
    rm "${pname2}/${fname2}"
    # exit 0
fi

echo "MCMC with task ${task} of subject ${sid}'s ROI ${output}."
start=`date +%s`
Rscript "${repo}/run_fsv_hcp.r" "${sid}" "${task}"  "${nfactor}" "${output}"
RESULT=$?
if [ $RESULT -eq 0 ]; then
    end=`date +%s`
    runtime=$((end-start))
    echo "MCMC FSV success."
    echo "Elapsed time: ${runtime}"

    exit 0
else
    echo "MCMC FSV failed."
    exit 1
fi
