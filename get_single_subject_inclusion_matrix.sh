#!/bin/bash

declare -a task_list=("REST1" "REST2" "EMOTION" "LANGUAGE" "MOTOR" "RELATIONAL" "SOCIAL" "GAMBLING" "WM")

phase="LR"
nfac=20

while getopts ":s:p:r:d:f" opt; do
  case $opt in
    s)
    sid=$OPTARG
    ;;
    p)
    phase=$OPTARG
    ;;
    r)
    repo=$OPTARG
    ;;
    d)
    datapath=$OPTARG
    ;;
    f)
    nfac=$OPTARG
    ;;
    *)
    echo "Invalid argument."
    ;;
  esac
done


echo "****      Subject ${sid}      ****"

fname_out="mcmc_f${nfac}_facload_${sid}.npz"
if [ -f "${datapath}/mcmc_fsv_facload/${fname_out}" ]; then
  echo "   - npz file already exists."
  exit 0
fi

for task in "${task_list[@]}"; do
  fname="mcmc_f${nfac}_${sid}_${task}.rdata"
  if [ ! -f "${datapath}/mcmc_fsv/${fname}" ]; then
    bash "${repo}/get_mcmc_data.sh" -s "${sid}" -t "${task}" -p "${phase}" -r "${repo}" -d "${datapath}" -f "${nfac}"
  fi # run mcmc if output not exists yet
done

python3 "${repo}/get_subject_inclusion_matrix.py" "${sid}" "${nfac}"

echo "****      Subject ${sid} completed.      ****"
exit 0