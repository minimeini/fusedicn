import os, sys
import re
import rdata
from nilearn import datasets, plotting
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm

if os.getcwd() not in sys.path:
    sys.path.append(os.getcwd())

import mcmc_fsv as fsv

def main(sid, nfac = 20):    
    nchan = 264

    outpath = os.path.join(fsv.dpath_root, "mcmc_fsv_facload")
    if not os.path.exists(outpath):
        os.mkdir(outpath)
    
    file_list = fsv.get_task_list(sid)
    print(">>> Subject {} has {} datasets: {}".format(sid, len(file_list), file_list))
    
    fname = "mcmc_f" + str(nfac) + "_facload_" + str(sid) + ".npz"
    fpath = os.path.join(fsv.dpath_root, "mcmc_fsv_facload", fname)
    
    if os.path.exists(fpath):
        print("Output .npz file already exists.")
        return
    
    if len(file_list) > 0:
        print("Try to compress data as npz.")
        try:
            tlab, Lambda, Zmap = fsv.get_nonzero_facload(
                sid, nchan, nfac, trimmed = True, verbose = False)
            print(tlab)
            np.savez(
                fpath, 
                tlab = tlab, file_list = file_list, Lambda = Lambda, Zmap = Zmap)
            print("Success.")
        except:
            print("Failed.")
    
    return


if __name__ == '__main__':
    sid = str(sys.argv[1])
    nfac = int(sys.argv[2])
    main(sid, nfac)