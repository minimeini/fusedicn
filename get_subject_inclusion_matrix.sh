#!/bin/bash
#SBATCH --job-name=hcp_fsv        # Job name
#SBATCH --output=out_%x_%j.txt           # Standard output file
#SBATCH --error=err_%x_%j.txt             # Standard error file
#SBATCH --nodes=1                     # Number of CPU cores
#SBATCH --ntasks=1                    # Run on a single CPU
#SBATCH --mem=8G
#SBATCH --time=24:00:00                # Maximum runtime (D-HH:MM:SS)
#SBATCH --mail-type=ALL               # Send email at job completion
#SBATCH --mail-user=mtang41@ucsc.edu    # Email address for notifications

nfac=$1

declare -a task_list=("REST1" "REST2" "EMOTION" "LANGUAGE" "MOTOR" "RELATIONAL" "SOCIAL" "GAMBLING" "WM")

phase="LR"

set +e

repo="${HOME}/repository/fusedicn"
if [[ "${USER}" == "mtang41" ]]; then 
    datapath="${HOME}/data"
    echo ">>>>>>>>>>>>>>   Setup Slurm environment."
    bash "${repo}/setup_slurm_env.sh"
else
    datapath="${HOME}/Dropbox/data"
fi

sublist="${repo}/info/hcp_subject_info.txt"

echo ">>>>>>>>>>>>>>   Get subject-specific inclusion probability matrix."

while read sid status; do
    echo "****      Subject ${sid}      ****"

    fname_out="mcmc_f${nfac}_facload_${sid}.npz"
    if [ -f "${datapath}/mcmc_fsv_facload/${fname_out}" ]; then
        echo "   - npz file already exists."
        continue
    fi

    ( for task in "${task_list[@]}"; do
        fname="mcmc_f${nfac}_${sid}_${task}.rdata"
        if [ ! -f "${datapath}/mcmc_fsv/${fname}" ]; then
            bash "${repo}/get_mcmc_data.sh" -s "${sid}" -t "${task}" -p "${phase}" -r "${repo}" -d "${datapath}" -f "${nfac}"
        fi # run mcmc if output not exists yet
    done ) &
    wait $!

    ( python3 "${repo}/get_subject_inclusion_matrix.py" "${sid}" "${nfac}" ) &
    wait $!

    echo "****      Subject ${sid} completed.      ****"

    if [ -f "${datapath}/mcmc_fsv_facload/${fname_out}" ]; then
        rm -r "${datapath}/mcmc_fsv/mcmc_f${nfac}_${sid}_*.rdata"
    fi

done < $sublist

echo ">>>>>>>>>>>>>>   Completed."

set -e
exit 0