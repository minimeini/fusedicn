function [sesInfo] = gift_voxel(ntime,nfac,varargin)

% change `group_modality` in `./icatb/icatb_defaults.m` to fMRI
% nvoxel = 214952
inputFilesPath = '../../../data/fMRI_nii';
% inputFilesPath = fullfile(getenv('HOME'),'data/fMRI_nii');
prefix = 'gica_voxel';
info_mat_path = '../info/gift_voxel_ica_parameter_info.mat';
reductPCT = 1.5;
blockPCT = 0.3;
outputDir = '../output/gica_voxel';

if nargin > 2, inputFilesPath = varargin{1}; end
if nargin > 3, prefix = varargin{2}; end
if nargin > 4, info_mat_path = varargin{3}; end
if nargin > 5, reductPCT = varargin{4}; end
if nargin > 6, blockPCT = varargin{5}; end
if nargin > 7, outputDir = varargin{6}; end

load(info_mat_path);
AllFiles = dir(inputFilesPath);
excludePattern = {'.' '..' '.DS_Store'};
excludePattern2 = {'.mat','.hdr','.log','.img','.txt','ica'};
AllFiles = AllFiles(~ismember({AllFiles.name},...
    excludePattern));

subjectFolder = struct('folder',[],'name',[]);
cnt = 0;
for i = 1:numel(AllFiles)
    flag = 1;
    for j = 1:numel(excludePattern2)
        if ~isempty(strfind(AllFiles(i).name,excludePattern2{j}))
            flag = 0;
            break;
        end
    end
    if flag == 1
        cnt = cnt + 1;
        subjectFolder(cnt).folder = AllFiles(i).folder;
        subjectFolder(cnt).name = AllFiles(i).name;
    end
end

nsub = numel(subjectFolder);

if exist(outputDir,'dir') == 0
    mkdir(outputDir);
end

ntimeTotal = sum(ntime(:));
%%
addpath("../../../software/GroupICAT/icatb");
addpath("../../../software/GroupICAT/icatb/icatb_analysis_functions");
addpath("../../../software/GroupICAT/icatb/icatb_analysis_functions/icatb_algorithms");
addpath("../../../software/GroupICAT/icatb/icatb_batch_files");
addpath("../../../software/GroupICAT/icatb/icatb_io_data_functions");
addpath("../../../software/GroupICAT/icatb/icatb_parallel_files");
addpath("../../../software/GroupICAT/icatb/icatb_scripts");
addpath("../../../software/GroupICAT/icatb/icatb_display_functions");
addpath("../../../software/GroupICAT/icatb/icatb_helper_functions");
addpath("../../../software/GroupICAT/icatb/icatb_mex_files");
addpath("../../../software/GroupICAT/icatb/icatb_spm_files");
addpath("../../../software/GroupICAT/icatb/icatb_mancovan_files");

%% Edit input files and output dir
sesInfo.outputDir = outputDir;
sesInfo.userInput.pwd = outputDir;
sesInfo.userInput.param_file = info_mat_path;

inputFiles = struct('name',[]);
for i = 1:nsub
    spath = fullfile(subjectFolder(i).folder,subjectFolder(i).name);
    sfiles = dir(spath);
    sfiles = sfiles(~ismember({sfiles.name},excludePattern));
    sfiles = fullfile({sfiles.folder},{sfiles.name});
    sfiles = icatb_rename_4d_file(sfiles);
    
    inputFiles(i).name = sfiles;
end

sesInfo.inputFiles = inputFiles;
sesInfo.userInput.files = inputFiles;

%% Change other variables
sesInfo.userInput.diffTimePoints = ones(1,nsub) .* ntimeTotal;
sesInfo.userInput.group_ica_type = 'spatial';
sesInfo.userInput.numOfSub = nsub;
sesInfo.userInput.numOfSess = 1;
sesInfo.userInput.numOfGroups1 = nsub;
sesInfo.userInput.numComp = nfac;
sesInfo.userInput.numReductionSteps = 2;
sesInfo.userInput.numOfPC1 = round(nfac*1.5);
sesInfo.userInput.numOfPC2 = nfac;
sesInfo.userInput.numofGroups2 = 1;
sesInfo.userInput.TR = 0.72;
sesInfo.userInput.backReconType = 'gica';
sesInfo.userInput.ICA_Options{20} = nfac;
% if sesInfo.userInput.ICA_Options{2} >= ntime
%     sesInfo.userInput.ICA_Options{2} = ceil(ntime * blockPCT);
% end
sesInfo.userInput.prefix = prefix;
sesInfo.userInput.parallel_info.mode = 'parallel';
sesInfo.userInput.parallel_info.num_workers = 12;
sesInfo.userInput.perfType = 'maximize performance';

sesInfo.group_ica_type = 'spatial';
sesInfo.numReductionSteps = 2;
sesInfo.numComp = nfac;
sesInfo.numOfSub = nsub;
sesInfo.numOfSess = 1;
sesInfo.numOfDataSets = nsub;
sesInfo.numOfScans = ntimeTotal;
sesInfo.TR = 0.72;
sesInfo.diffTimePoints = ones(1,nsub) .* ntimeTotal;
sesInfo.backReconType = 'gica';
sesInfo.ICA_Options = sesInfo.userInput.ICA_Options;
sesInfo.parallel_info = sesInfo.userInput.parallel_info;
% sesInfo.mask_ind = sesInfo.userInput.mask_ind;

sesInfo.reduction(1).numOfGroupsBeforeCAT = nsub;
sesInfo.reduction(1).numOfGroupsAfterCAT = nsub;
sesInfo.reduction(1).numOfPCBeforeCAT = ones(1,nsub) .* ntimeTotal;
sesInfo.reduction(1).numOfPCAfterReduction = sesInfo.userInput.numOfPC1;
sesInfo.reduction(1).numOfPrevGroupsInEachNewGroupAfterCAT = ones(1,nsub);
sesInfo.reduction(1).numOfPCInEachGroupAfterCAT = ones(1,nsub) .* ntimeTotal;

sesInfo.reduction(2).numOfGroupsBeforeCAT = nsub;
sesInfo.reduction(2).numOfGroupsAfterCAT = 1;
sesInfo.reduction(2).numOfPCBeforeCAT = sesInfo.userInput.numOfPC1;
sesInfo.reduction(2).numOfPCAfterReduction = sesInfo.userInput.numOfPC2;
sesInfo.reduction(2).numOfPrevGroupsInEachNewGroupAfterCAT = nsub;
sesInfo.reduction(2).numOfPCInEachGroupAfterCAT = ...
    sesInfo.reduction(2).numOfPCBeforeCAT * ...
    sesInfo.reduction(2).numOfPrevGroupsInEachNewGroupAfterCAT;

sesInfo.reduction(3).numOfGroupsBeforeCAT = 1;
sesInfo.reduction(3).numOfGroupsAfterCA = -1;
sesInfo.reduction(3).numOfPCBeforeCAT = nfac;
sesInfo.reduction(3).numOfPCAfterReduction = 0;



%%
sesInfo = icatb_runAnalysis(sesInfo,1);
%%
save(info_mat_path,'sesInfo');

end