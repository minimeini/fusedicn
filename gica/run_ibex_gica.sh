#!/bin/bash
#SBATCH -N 1
#SBATCH -n 12
#SBATCH --partition=batch
#SBATCH -J gica
#SBATCH -o gica.%J.out
#SBATCH -e gica.%J.err
#SBATCH --mail-user=meini.tang@kaust.edu.sa
#SBATCH --mail-type=ALL
#SBATCH --time=8:00:00
#SBATCH --mem=200G

module load matlab/R2020a
matlab -nosplash -nodesktop -nodisplay -nojvm -r "gift_voxel([1200 316],20);exit"
