#!/bin/bash
set -e

nsub=${1:-200}
isibex=${2:-0}

if [[ $isibex -eq 0 ]]
then
	unameOut="$(uname -s)"
	case "${unameOut}" in
    	Linux*)     pfix="/home/tangm0a";;
    	Darwin*)    pfix="/Users/meini";;
    	*)          pfix="/UNKNOWN"
	esac
else
	pfix="/ibex/scratch/tangm0a"
fi

# create fMRI_nii main folder and individual subfolder
data_input_rest="${pfix}/data/rfMRI_ts/REST1"
data_input_task="${pfix}/data/tfMRI_ts/LANGUAGE"
data_output="${pfix}/data/fMRI_nii"

if [ ! -d "${data_output}" ]
then
	mkdir "${data_output}"
fi

pfix_rest="rfMRI_ts_REST1_LR_"
pfix_task="tfMRI_ts_LANGUAGE_LR_"

sub_rest=($(ls ${data_input_rest}))
id_rest=()
for srest in ${sub_rest[@]}
do
	rid=$(echo $srest | awk -F_ '{ print $5 }')
	rid=$(echo $rid | awk -F. '{ print $1 }')
	id_rest+=($rid)
done

sub_task=($(ls ${data_input_task}))
id_task=()
for stask in ${sub_task[@]}
do
	rid=$(echo $stask | awk -F_ '{ print $5 }')
	rid=$(echo $rid | awk -F. '{ print $1 }')
	id_task+=($rid)
done

# Get common sub ids
sid=()
for item1 in "${id_rest[@]}"; do
    for item2 in "${id_task[@]}"; do
        if [[ $item1 = $item2 ]]; then
            sid+=("$item1")
        fi
    done
done

totalSub=${#sid[@]}
nsub=$(echo $((${nsub}>${totalSub} ? ${totalSub} : ${nsub})))
declare -i cnt=0
for id in "${sid[@]}"
do
	cnt=$(($cnt+1))
	if [ "${cnt}" -gt "${nsub}" ]
	then
		break
	fi

	spath="${data_output}/${id}"
	if [ ! -d "${spath}" ]
	then
		mkdir ${spath}
	fi

	file="${spath}/${pfix_rest}${id}.nii"
	if [ ! -f "${file}" ]
	then
    	cp "${data_input_rest}/${pfix_rest}${id}.nii.gz" "${file}.gz"
		gunzip "${file}.gz"
	fi

	file="${spath}/${pfix_task}${id}.nii"
	if [ ! -f "${file}" ]
	then
    	cp "${data_input_task}/${pfix_task}${id}.nii.gz" "${file}.gz"
		gunzip "${file}.gz"
	fi
done


set +e