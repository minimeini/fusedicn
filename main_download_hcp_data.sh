#!/bin/bash
set +e

eval "$(conda "shell.$(basename "${SHELL}")" hook)"
conda activate hbenv

task=$1
phase=$2


# pfix=$(realpath ~)
repo="${HOME}/repository/fusedicn"
datapath="${HOME}/data"
sublist="${repo}/info/hcp_subject_info.txt"


hcp_id="welkin31"
hcp_pswd="hetcube31"
# Credentials of [Connectomedb](https://db.humanconnectome.org/app/template/Login.vm).



while read sid status; do
	bash "${repo}/preprocessing/get_data.sh" -s ${sid} -t "${task}" -p "${phase}" -u "${hcp_id}:${hcp_pswd}" -r "${repo}" -d "${datapath}"
done < $sublist


wait

set -e
exit 0