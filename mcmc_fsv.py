import os
import re
import glob
import pdb
import rdata
from nilearn import datasets, plotting
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm

if os.environ.get('USER') == "meinitang":
    dpath_root = os.path.join(os.path.expanduser("~"), "data")
elif os.environ.get('USER') == "mtang41":
    dpath_root = os.path.join(os.path.expanduser("~"), "data")

dpath_data = dpath_root + "/mcmc_fsv"

task_list = ["REST1", "REST2", "LANGUAGE", "GAMBLING", "EMOTION", "RELATIONAL", "SOCIAL", "MOTOR", "WM"]
mod_list = ["rfMRI", "rfMRI", "tfMRI", "tfMRI", "tfMRI", "tfMRI", "tfMRI", "tfMRI", "tfMRI"]
phase = "LR"

def default_settings():
    return task_list, mod_list, phase

def subplot_markers(val, coords, vbnd, fig, axes, node_size = 10, title = None):
    img = plotting.plot_markers(
        val, coords, 
        node_cmap = cm.bwr, node_size = node_size,
        node_vmin = -vbnd, node_vmax = vbnd,
        figure = fig, axes = axes, 
        title = title, colorbar = False)
    return img

def fetch_power_atlas_coords():
    """
    Fetch Power atlas coordinates and return them.
    """
    power = datasets.fetch_coords_power_2011()
    coords = np.vstack((power.rois['x'], power.rois['y'], power.rois['z'])).T
    return coords


def get_task_list(sid, ftype = "mcmc", in_type = "timeseries"):
    task_list, mod_list, phase = default_settings()
    if ftype == "mcmc":
        dpath = dpath_data
        dfiles = os.listdir(dpath)
        dfiles = [f for f in dfiles if str(sid) in f and in_type in f]
        stask = [f.split("_")[-2].split(".")[0] for f in dfiles]
        
    elif ftype == "data":
        stask = []
        for task in task_list:
            idx = task_list.index(task)
            mod = mod_list[idx]
            dpath = os.path.join(dpath_root, mod, task)
            dfile = "_".join(mod, task, phase, str(sid), in_type) + ".txt"
            if os.path.exists(os.path.join(dpath, dfile)):
                stask.append(task)
    else:
        print("Unknown task type")
        return 1
            
    stask = set(stask)
    return stask



def get_sid_list(task : str, nfac : int = None, ftype : str = "mcmc", in_type : str = "timeseries"):
    task_list, mod_list, phase = default_settings()
    if ftype == "mcmc":
        dpath = dpath_data
        dfiles = os.listdir(dpath)
        if nfac is not None:
            flab = f"_f{nfac}_"
            dfiles = [f for f in dfiles if str(task) in f and in_type in f and flab in f]
        else:
            dfiles = [f for f in dfiles if str(task) in f and in_type in f]
        
        stask = [int(f.split("_")[2]) for f in dfiles]
    # elif ftype == "data":
    #     stask = []
    #     for task in task_list:
    #         idx = task_list.index(task)
    #         mod = mod_list[idx]
    #         dpath = os.path.join(dpath_root, mod, task)
    #         dfile = "_".join(mod, task, phase, str(sid), in_type) + ".txt"
    #         if os.path.exists(os.path.join(dpath, dfile)):
    #             stask.append(task)
    else:
        print("Unknown task type")
        return 1
            
    stask = set(stask)
    return stask
        


    


def load_data(sid, task, in_type = "timeseries", load_data = True, load_mcmc = True):
    task_list, mod_list, phase = default_settings()
    
    f1 = "mcmc_fsv_" + str(sid) + "_" + str(task) + "_" + in_type + ".rdata"
    mcmc_file = os.path.join(dpath_data, f1)
    mcmc_exists = os.path.exists(mcmc_file)
    
    mcmc = None
    if mcmc_exists and load_mcmc:
        mcmc = rdata.parser.parse_file(mcmc_file)
        mcmc = rdata.conversion.convert(mcmc)
        mcmc = mcmc["output"]
    
    idx = task_list.index(str(task))
    mod = str(mod_list[idx])
    f2 = mod + "/" + str(task) + "/" + mod + "_" + str(task) + "_" + str(phase) + "_" + str(sid) + "_" + in_type + ".txt"
    data_file = os.path.join(dpath_root, f2)
    data_exists = os.path.exists(data_file)
    
    data = None
    if data_exists and load_data:
        data = np.loadtxt(data_file)
    
    return data, mcmc
    


def get_nonzero_facload(sid, nchan = 264, nfac = 20, trimmed = True, verbose = True, in_type = "timeseries"):
    task_list, mod_list, phase = default_settings()

    status = []
    mcmc_list = []
    data_list = []
    for i in range(len(task_list)):
        task = task_list[i]
        mod = mod_list[i]

        f1 = "mcmc_f" + str(nfac) + "_" + str(sid) + "_" + task + "_" + in_type + ".rdata"
        f1 = os.path.join(dpath_data, f1)
        # fn_old = "mcmc_fsv_" + str(sid) + "_" + task + ".rdata"
        # fn_old = os.path.join(dpath_data, fn_old)
        
        mcmc_file = ""
        data_file = ""
        
        if os.path.exists(f1):
            print(f"MCMC output of {f1} exists.")
            mcmc_file = f1
        # elif os.path.exists(fn_old):
        #     mcmc_file = fn_old
        else:
            if verbose:
                print(f"MCMC output of {f1} doesn't exist.")
            
            ss = False
            status += [ss]
            mcmc_list += [mcmc_file]
            data_list += [data_file]
            
            continue
        
        
        mcmc_list += [mcmc_file]
        
        f2 = mod + "/" + task + "/" + mod + "_" + task + "_" + phase + "_" + str(sid) + "_" + in_type + ".txt"
        data_file = os.path.join(dpath_root, f2)
        data_list += [data_file]

        ss = os.path.exists(mcmc_file) and os.path.exists(data_file)
        status += [ss]
    
    tlab = [task_list[it] for it in range(len(task_list)) if status[it]]
        
    ntask = sum(status)
    if ntask == 0:
        Lambda = np.zeros((nchan, nfac, len(task_list)))
        Zmap = np.zeros((nchan, nfac, len(task_list)))
        return tlab, Lambda, Zmap
    
    Lam_list = []
    zlist = []
    shape = np.zeros(3)
    for i in range(len(task_list)):
        # concatenate across task for the same subject
        if status[i]:
            mcmc = rdata.parser.parse_file(mcmc_list[i])
            mcmc = rdata.conversion.convert(mcmc)
            lam = mcmc["Lambda"] # Lambda: nroi x nfac x nsample
            ztmp = mcmc["Zmap"]
            
            Lam_list += [np.atleast_2d(lam)]
            zlist += [np.atleast_2d(ztmp)]
            
            shape[0] = int(np.max([shape[0], lam.shape[0]]))
            shape[1] = int(np.max([shape[1], lam.shape[1]]))
            
    shape[2] = len(Lam_list)
    Lambda = np.zeros(shape.astype(int))
    Zmap = np.zeros(shape.astype(int))
    for i in range(int(shape[2])):
        Lambda[0:(Lam_list[i].shape[0]), 0:(Lam_list[i].shape[1]), i] = Lam_list[i]
        Zmap[0:(zlist[i].shape[0]), 0:(zlist[i].shape[1]), i] = zlist[i]
    
    # Lambda = np.concatenate(Lam_list, axis = 2)
    # Zmap = np.concatenate(zlist, axis = 2)
    
    if trimmed:
        Lambda = np.squeeze(Lambda)
        Zmap_all = np.squeeze(Zmap)
        Zmap = np.squeeze(Zmap.astype(float))
        
        if len(Zmap.shape) > 2:
            Zmap0 = np.apply_over_axes(np.sum, Zmap, 2)
            Zmap = np.squeeze(Zmap0.astype(float))
            Zmap /= float(len(tlab))
        
        return tlab, Lambda, Zmap_all, Zmap
    else:
        return tlab, Lambda, Zmap




def get_nonzero_facload_by_task(
    task : str, 
    nfac : int = 20, 
    in_type : str = "timeseries",
    trimmed : bool = True, 
    verbose : bool = True):
    
    task_list, mod_list, phase = default_settings()
    sid_list = list(get_sid_list(task, nfac=nfac, ftype='mcmc', in_type=in_type))
    if len(sid_list) == 0:
        return 0
    
    if 'REST' in task:
        mod = 'rfMRI'
    else:
        mod = 'tfMRI'

    status = []
    mcmc_list = []
    data_list = []
    for i in range(len(sid_list)):
        sid = sid_list[i]

        f1 = "mcmc_f" + str(nfac) + "_" + str(sid) + "_" + task + "_" + in_type + ".rdata"
        f1 = os.path.join(dpath_data, f1)
        # fn_old = "mcmc_fsv_" + str(sid) + "_" + task + ".rdata"
        # fn_old = os.path.join(dpath_data, fn_old)
        
        mcmc_file = ""
        data_file = ""
        
        if os.path.exists(f1):
            mcmc_file = f1
        # elif os.path.exists(fn_old):
        #     mcmc_file = fn_old
        else:
            ss = False
            status += [ss]
            mcmc_list += [mcmc_file]
            data_list += [data_file]
            
            continue
        
        
        mcmc_list += [mcmc_file]
        
        f2 = mod + "/" + task + "/" + mod + "_" + task + "_" + phase + "_" + str(sid) + "_" + in_type + ".txt"
        data_file = os.path.join(dpath_root, f2)
        data_list += [data_file]

        ss = os.path.exists(mcmc_file) and os.path.exists(data_file)
        status += [ss]
            
    nsub = sum(status)
    if nsub == 0:
        return 0
    
    print(f"{nsub} subjects for {task} of {nfac} factors.")
    
    Lam_list = []
    zlist = []
    metric = []
    shape = np.zeros(3)
    for i in range(len(sid_list)):
        # concatenate across task for the same subject
        if status[i]:
            mcmc = rdata.parser.parse_file(mcmc_list[i])
            mcmc = rdata.conversion.convert(mcmc)
            lam = mcmc["Lambda"] # Lambda: nroi x nfac x nsample
            ztmp = mcmc["Zmap"]
            
            Lam_list += [np.atleast_2d(lam)]
            zlist += [np.atleast_2d(ztmp)]
            metric += [mcmc['metrics']]
            
            shape[0] = int(np.max([shape[0], lam.shape[0]]))
            shape[1] = int(np.max([shape[1], lam.shape[1]]))
    
    if len(metric) > 1:
        metric = np.concatenate(metric, axis = 1)
    elif len(metric) == 1:
        metric = metric[0]
    
    shape[2] = len(Lam_list)
    Lambda = np.zeros(shape.astype(int))
    Zmap = np.zeros(shape.astype(int))
    for i in range(int(shape[2])):
        Lambda[0:(Lam_list[i].shape[0]), 0:(Lam_list[i].shape[1]), i] = Lam_list[i]
        Zmap[0:(zlist[i].shape[0]), 0:(zlist[i].shape[1]), i] = zlist[i]
    
    # Lambda = np.concatenate(Lam_list, axis = 2)
    # Zmap = np.concatenate(zlist, axis = 2)
    
    if trimmed:
        Lambda = np.squeeze(Lambda)
        Zmap_all = np.squeeze(Zmap)
        Zmap = np.squeeze(Zmap.astype(float))
        
        if len(Zmap.shape) > 2:
            Zmap0 = np.apply_over_axes(np.sum, Zmap, 2)
            Zmap = np.squeeze(Zmap0.astype(float))
            Zmap /= float(nsub)
        
        return Lambda, Zmap_all, Zmap, metric
    else:
        return Lambda, Zmap, metric




coords = fetch_power_atlas_coords()
qtls = np.asarray([0.025, 0.5, 0.975])

