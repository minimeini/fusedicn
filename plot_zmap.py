#!/usr/bin/env python
# coding: utf-8

# In[235]:


import os
import re
import rdata
from nilearn import datasets, plotting
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm

dpath = os.path.expanduser("~") + "/data"
d1 = dpath + "/mcmc_fsv"

sid = "101309"
qtls = np.asarray([0.025, 0.5, 0.975])

nchan = 264
nfac = 20

def default_settings():
    task_list = ["REST1", "REST2", "LANGUAGE", "GAMBLING", "EMOTION", "RELATIONAL", "SOCIAL", "MOTOR", "WM"]
    mod_list = ["rfMRI", "rfMRI", "tfMRI", "tfMRI", "tfMRI", "tfMRI", "tfMRI", "tfMRI", "tfMRI"]
    phase = "LR"
    return task_list, mod_list, phase

def subplot_markers(val, coords, vbnd, fig, axes, node_size = 10, title = None):
    img = plotting.plot_markers(
        val, coords, 
        node_cmap = cm.bwr, node_size = node_size,
        node_vmin = -vbnd, node_vmax = vbnd,
        figure = fig, axes = axes, 
        title = title, colorbar = False)
    return img

def fetch_power_atlas_coords():
    """
    Fetch Power atlas coordinates and return them.
    """
    power = datasets.fetch_coords_power_2011()
    coords = np.vstack((power.rois['x'], power.rois['y'], power.rois['z'])).T
    return coords

coords = fetch_power_atlas_coords()


# In[218]:


def get_task_list(sid, ftype = "mcmc"):
    task_list, mod_list, phase = default_settings()
    if ftype == "mcmc":
        dpath = os.path.join(os.path.expanduser("~"), "data", "mcmc_fsv")
        dfiles = os.listdir(dpath)
        dfiles = [f for f in dfiles if str(sid) in f]
        stask = [f.split("_")[-1].split(".")[0] for f in dfiles]
        
    elif ftype == "data":
        stask = []
        for task in task_list:
            idx = task_list.index(task)
            mod = mod_list[idx]
            dpath = os.path.join(os.path.expanduser("~"), "data", mod, task)
            dfile = "_".join(mod, task, phase, str(sid), "timeseries.txt")
            if os.path.exists(os.path.join(dpath, dfile)):
                stask.append(task)
    else:
        print("Unknown task type")
        return 1
            
    stask = set(stask)
    return stask
        



def get_sid_list(dpath, pattern = ""):
    dfiles = os.listdir(dpath)
    dfiles = [f for f in dfiles if pattern in f]
    sid_list = []
    for f in dfiles:
        sid = re.findall(r'\d+',f)
        if sid is not None:
            if len(sid) > 1:
                sid = sid[-1]
            
            if type(sid) == list:
                sid = sid[0]
            
            if len(sid) > 1:
                sid_list.append(sid)
    
    sid_list = set(sid_list)
    return sid_list
    

def get_status(check_input = True, check_mcmc = False):
    task_list, mod_list, phase = default_settings()
    ntask = len(task_list)
    
    sid_list = []
    for i in range(ntask):
        if check_input:
            dpath = os.path.join(os.path.expanduser("~"), "data", mod_list[i], task_list[i])
            sid_list += get_sid_list(dpath, "timeseries")
    
    sid_list = set(sid_list)
    
    if check_mcmc:
        dpath = os.path.join(os.path.expanduser("~"), "data", "mcmc_fsv")
        sid_list_out = get_sid_list(dpath, "rdata")
        sid_list = sid_list & sid_list_out
    
    return sid_list
    


def load_data(sid, task, load_data = True, load_mcmc = True):
    task_list, mod_list, phase = default_settings()
    
    f1 = "mcmc_fsv_" + str(sid) + "_" + str(task) + ".rdata"
    mcmc_file = os.path.join(dpath, "mcmc_fsv", f1)
    mcmc_exists = os.path.exists(mcmc_file)
    
    mcmc = None
    if mcmc_exists and load_mcmc:
        mcmc = rdata.parser.parse_file(mcmc_file)
        mcmc = rdata.conversion.convert(mcmc)
        mcmc = mcmc["output"]
    
    idx = task_list.index(str(task))
    mod = str(mod_list[idx])
    f2 = mod + "/" + str(task) + "/" + mod + "_" + str(task) + "_" + str(phase) + "_" + str(sid) + "_timeseries.txt"
    data_file = os.path.join(dpath, f2)
    data_exists = os.path.exists(data_file)
    
    data = None
    if data_exists and load_data:
        data = np.loadtxt(data_file)
    
    return data, mcmc
    


def get_nonzero_facload(sid, nchan = 264, nfac = 20, trimmed = True):
    task_list, mod_list, phase = default_settings()

    status = []
    mcmc_list = []
    data_list = []
    for i in range(len(task_list)):
        task = task_list[i]
        mod = mod_list[i]

        f1 = "mcmc_fsv_" + sid + "_" + task + ".rdata"
        mcmc_file = os.path.join(dpath, "mcmc_fsv", f1)
        mcmc_list += [mcmc_file]

        f2 = mod + "/" + task + "/" + mod + "_" + task + "_" + phase + "_" + sid + "_timeseries.txt"
        data_file = os.path.join(dpath, f2)
        data_list += [data_file]

        ss = os.path.exists(mcmc_file) and os.path.exists(data_file)
        status += [ss]
    
    Lambda = np.zeros((nchan, nfac, len(task_list)))
    Zmap = np.zeros((nchan, nfac, len(task_list)))
    tlab = []
    
    ntask = sum(status)
    if ntask == 0:
        return tlab, Lambda, Zmap
    

    for i in range(len(task_list)):
        if status[i]:
            mcmc = rdata.parser.parse_file(mcmc_list[i])
            mcmc = rdata.conversion.convert(mcmc)
            mcmc = mcmc["output"]
            lam = mcmc["facload"]

            for j in range(nchan):
                for k in range(nfac):
                    dat = lam[j, k, :]
                    dat = dat.flatten()
                    nonzero = np.unique(np.sign(np.quantile(dat, qtls))).shape[0] == 1
                    val = np.mean(dat)
                    nonzero = nonzero and abs(val) > 1.e-8
                    
                    Zmap[j, k, i] = bool(nonzero)
                    Lambda[j, k, i] = val * float(nonzero)
    
    tid = [i for i in range(len(task_list)) if status[i]]
    tlab = [task_list[it] for it in range(len(task_list)) if status[it]]
    
    if trimmed:
        Lambda = Lambda[:,:,tid]
        Zmap = np.squeeze(Zmap[:,:,tid])
        
        if len(Zmap.shape) > 2:
            Zmap0 = np.apply_over_axes(np.sum, Zmap, 2)
            Zmap = np.squeeze(Zmap0)
            Zmap /= len(tlab)


    return tlab, Lambda, Zmap



# ## Group by Subject

# In[268]:


def plot_factor_by_subject(lam_stat, task_labels, factor_id = 0):
    ntask = len(task_labels)
    nfac = lam_stat.shape[1]
    vbnd = np.max(np.abs(lam_stat.flatten()))
    
    fig, axs = plt.subplots(ntask)
    for i in range(ntask):
        ltmp = lam_stat[:, factor_id, i]
        subplot_markers(ltmp, coords, vbnd, fig, axs[i], 10, title = task_labels[i])

    return fig, axs
    # fig.savefig(fig_file)


# In[6]:


nfac = 20
plt.rcParams["font.size"] = "8"
sid_list = get_status(True, True)

for id in sid_list:
    tlab, Lammap, Zmap = get_nonzero_facload(id, nchan, nfac)
    vbnd = np.max(np.abs(Lammap.flatten()))
    ngrp = len(tlab)
    
    for k in range(nfac):
        fn = "mcmc_fsv" + str(k) + "_" + str(id)
        
        fig1 = plt.figure()
        plotting.plot_markers(Zmap[:, k], coords, node_cmap = cm.binary, node_vmin = 0, node_vmax = 1, figure = fig1, colorbar=False, title=None)
        fn1 = os.path.join(dpath, "mcmc_fsv_markers", fn + "_1.png")
        fig1.savefig(fn1, dpi = 300, bbox_inches = 'tight', pad_inches = 0)
        plt.close()
        
        if ngrp > 1:
            fig2, axs = plot_factor_by_subject(Lammap, tlab, k)
            fn2 = os.path.join(dpath, "mcmc_fsv_markers", fn + "_2.png")
        else:
            fig2 = plt.figure()
            plotting.plot_markers(Lammap[:,k, 0], coords, node_cmap = cm.bwr, node_vmin = -vbnd, node_vmax = vbnd, figure = fig2, colorbar=False, title=tlab[0])
            
        fig2.savefig(fn2, dpi = 300, bbox_inches = 'tight', pad_inches = 0)
        plt.close()
        
        fig, (ax1, ax2) = plt.subplots(2)
        
        ax1.imshow(plt.imread(fn1))
        ax1.set_frame_on(False)
        ax1.axis('off')
        ax2.imshow(plt.imread(fn2))
        ax2.set_frame_on(False)
        ax2.axis('off')
        fig.subplots_adjust(wspace=0, hspace=0)
        fig.savefig(os.path.join(dpath, "mcmc_fsv_markers", fn + ".png"), dpi = 300)
        
        plt.close()
        os.remove(fn1)
        os.remove(fn2)
        
        
        


# ## Group by Task

# In[3]:


def get_nonzero_facload2(task, nchan = 264, nfac = 20, trimmed = True):
    task_list, mod_list, phase = default_settings()
    mod = mod_list[task_list.index(task)]
    fpath = os.path.join(dpath, "mcmc_fsv")
    sid_list = get_sid_list(fpath, pattern = task + ".rdata")

    Lambda = None
    Zmap = None 
    if not len(sid_list) >= 1:
        return sid_list, Lambda, Zmap
    
    Lambda = np.zeros((nchan, nfac, len(task_list)))
    Zmap = np.zeros((nchan, nfac, len(task_list)))
    
    for i in range(len(sid_list)):
        fn = os.path.join(fpath, "_".join(["mcmc_fsv", str(*sid), task + ".rdata"]))
        if os.path.exists(fn):
            mcmc = rdata.parser.parse_file(fn)
            mcmc = rdata.conversion.convert(mcmc)
            mcmc = mcmc["output"]
            lam = mcmc["facload"]

            for j in range(nchan):
                for k in range(nfac):
                    dat = lam[j, k, :]
                    dat = dat.flatten()
                    nonzero = np.unique(np.sign(np.quantile(dat, qtls))).shape[0] == 1
                    val = np.mean(dat)
                    nonzero = nonzero and abs(val) > 1.e-8
                    
                    Zmap[j, k, i] = bool(nonzero)
                    Lambda[j, k, i] = val * float(nonzero)
    
    if trimmed:
        Zmap = np.squeeze(Zmap)
        
        if len(Zmap.shape) > 2:
            nsub = Zmap.shape[2]
            Zmap0 = np.apply_over_axes(np.sum, Zmap, 2)
            Zmap = np.squeeze(Zmap0)
            Zmap /= nsub
        
    return sid_list, Lambda, Zmap


# In[5]:

task_list, mod_list, phase = default_settings()
for task in task_list:
    sid_list, Lammap, Zmap = get_nonzero_facload2(task)
    
    if Zmap is not None:
        for k in range(nfac):
            fn = "mcmc_fsv" + str(k) + "_" + task + ".png"
        
            fig1 = plt.figure()
            plotting.plot_markers(Zmap[:, k], coords, node_cmap = cm.binary, node_vmin = 0, node_vmax = 1, figure = fig1, colorbar=False, title=task)
            fig1.savefig(os.path.join(dpath, fn), dpi = 300, bbox_inches = 'tight', pad_inches = 0)
            plt.close()

