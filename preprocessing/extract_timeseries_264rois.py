#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modified on Sat Feb 17 2024
@author: Sin-Yee YAP (sin.yap@monash.edu)

To extract the time series using Power atlas (264 ROIs) as masker.

Bash command to run:  ./extract_264rois.py <fmri_type> <TASK>
- fmri_type: rfMRI or tfMRI
- TASK: (eg - REST1)
    
"""

import numpy as np
import pandas as pd
import nibabel as nib
from nilearn import datasets, input_data
import sys  # Import sys to handle command line arguments

def load_nifti_file(file_path):
    """
    Load and return a NIFTI file.
    """
    return nib.load(file_path)

def fetch_power_atlas_coords():
    """
    Fetch Power atlas coordinates and return them.
    """
    power = datasets.fetch_coords_power_2011()
    coords = np.vstack((power.rois['x'], power.rois['y'], power.rois['z'])).T
    return coords

def compute_timeseries(nifti_file, coords):
    """
    Compute and return the time series for a given NIFTI file and atlas coordinates.
    """
    masker = input_data.NiftiSpheresMasker(
        seeds=coords,
        smoothing_fwhm=6,
        radius=5.,
        detrend=True,
        standardize=True,
        low_pass=0.1,
        high_pass=0.01,
        t_r=2
    )
    timeseries = masker.fit_transform(nifti_file)
    return timeseries


if __name__ == '__main__':
    file_path = str(sys.argv[1])
    print(f'Processing file: {file_path}...')

    coords = fetch_power_atlas_coords()
    nifti_file = load_nifti_file(file_path)
    timeseries = compute_timeseries(nifti_file, coords)

    base_name = file_path[:-7]  # Remove the .nii.gz extension
    # file_name = f'{base_name}_timeseries.txt'  # Use the base name for saving
    file_name = file_path[:-7] + '_timeseries.txt'
    np.savetxt(file_name, timeseries, fmt='%f')

