#!/bin/bash

CACHED=false
output="timeseries"

while getopts ":s:t:p:u:r:d:co:" opt; do
  case $opt in
    s)
    sid=$OPTARG
    ;;
    t)
    task=$OPTARG
    ;;
    p)
    phase=$OPTARG
    ;;
    u)
    hcp_id=$(echo $OPTARG | awk -F':' '{ print $1 }')
    hcp_pswd=$(echo $OPTARG | awk -F':' '{ print $2 }')
    ;;
    r)
    repo=$OPTARG
    ;;
    d)
    datapath=$OPTARG
    ;;
    c)
    CACHED=false
    ;;
    o)
    output=$OPTARG # timeseries, mean, median, mode
    ;;
    *)
    echo "Invalid argument."
    ;;
  esac
done


# Columns of the stimulus (ev) text file
# Blocked type: onset, duration, amplitude
# Event type  : onset

case ${task} in
	EMOTION)        # blocked; ev columns: onset, duration, amplitude
	filelist=("fear" "neut")
    # filelist=("fear")
	;;
	LANGUAGE)       # blocked
	filelist=("math" "story")
    # filelist=("math")
	;;
	MOTOR)          # blocked 
	filelist=("cue" "lf" "lh" "rf" "rh" "t")
    # filelist=("lf" "lh" "rf" "rh" "t")
	;;
	RELATIONAL)      # blocked
	filelist=("err" "match" "relation")
    # filelist=("match" "relation")
	;;
	SOCIAL)          # blocked + event
	filelist=("mental" "mental_resp" "rnd" "rnd_resp")
    # filelist=("mental" "mental_resp" "rnd")
	;;
    GAMBLING)        # blocked for win loss, event for outcome: win, loss, neut
	filelist=("loss" "win" "win_event" "loss_event" "neut_event")
    # filelist=("loss" "win" "win_event" "loss_event")
	;;
	WM)              # blocked for objects, event for cor[rect], err[or], 
	filelist=("0bk_body" "0bk_cor" "0bk_err" "0bk_faces" "0bk_places" "0bk_tools" "2bk_body" "2bk_cor" "2bk_err" "2bk_faces" "2bk_places" "2bk_tools")
    # filelist=("0bk_body" "0bk_cor" "0bk_err" "0bk_faces" "0bk_places" "0bk_tools" "2bk_body" "2bk_cor" "2bk_err" "2bk_faces" "2bk_places")
	;;
	*)
	;;
esac


pfix=$(realpath ~)

if [ -z $hcp_id ] || [ -z $hcp_pswd ]; then 
    echo "Error: HCP user id and password are required."
    exit 1
fi

if [ -z $sid ]; then sid="100206"; fi
if [ -z $task ]; then task="REST1"; fi
if [ -z $phase ]; then phase="LR"; fi
if [ -z $repo ]; then
    repo="${HOME}/repository/fusedicn"
    echo ">>> Default repository path: ${repo}."
fi
if [ ! -d ${repo} ]; then
	echo "Error: Repository fusedicn not found."
    exit 1
fi

if [ -z $datapath ]; then
    datapath="${HOME}"
    echo ">>> Default data path: ${datapath}"
fi
if [ ! -d ${datapath} ]; then
	mkdir ${datapath}
fi

roi_name="AAL90"
roi_input="${datapath}/roi_mask/${roi_name}" # roi_input="~/Dropbox/data/roi_mask/AAL90"
if [ ! -d ${roi_input} ]; then
    echo "Error: ROI masks not found."
    exit 1
fi


# Preprocessed tfMRI data
# URL: "https://db.humanconnectome.org/data/archive/projects/HCP_1200/subjects/100206/experiments/100206_3T/resources/tfMRI_LANGUAGE_LR_preproc/files/MNINonLinear/Results/tfMRI_LANGUAGE_LR/tfMRI_LANGUAGE_LR.nii.gz"
# Downloaded file name: tfMRI_${task}_LR.nii.gz

if [[ "${task}" =~ REST ]]; then
  dl_type="rfMRI"
  preproc="FIX"
  dpath="files"
  postfix="_hp2000_clean.nii.gz"
else
  dl_type="tfMRI"
  preproc="preproc"
  dpath="files/MNINonLinear/Results"
  postfix=".nii.gz"
fi

label="${dl_type}_${task}_${phase}"
url_lv1="https://db.humanconnectome.org/data/archive/projects/HCP_1200/subjects"
url_lv2="${sid}/experiments/${sid}_3T/resources"
url_lv3="${label}_${preproc}/${dpath}/${label}"
url="${url_lv1}/${url_lv2}/${url_lv3}"

savepath="${datapath}/${dl_type}"
if [ ! -d ${savepath} ]; then mkdir ${savepath}; fi
cd $savepath


spath="${savepath}/${task}" # spath="~/Dropbox/data/rfMRI/REST1"
if [ ! -d ${spath} ]; then mkdir ${spath}; fi
cd $spath




echo ">>>>      Subject ${sid} - ${dl_type} - ${task}"


url_fname="${label}${postfix}"
url_full="${url}/${url_fname}"

fname="${label}_${sid}_raw.nii.gz"
fraw="${spath}/${fname}"


fmed_name="${spath}/${label}_${sid}_${output}.txt"
fmed_size=$( ${repo}/preprocessing/get_file_size.sh "${fmed_name}" )
if [ -f "${fmed_name}" ] && [ ${fmed_size} -gt 100 ]; then 
    echo "   - Processing done."
    exit 0
fi

fmed2_name="${spath}/${label}_${output}_${sid}.txt"
fmed2_size=$( ${repo}/preprocessing/get_file_size.sh "${fmed2_name}" )
if [ -f "${fmed2_name}" ] && [ ${fmed2_size} -gt 100 ]; then 
    echo "   - Processing done; rename output."

    mv ${fmed2_name} ${fmed_name}
    if [ $? -ne 0 ]; then
        echo "      Failed."
        exit 1
    else
        exit 0
    fi
fi

[ -f "${fraw}" ] && rm ${fraw}
[ -f "${fmed_name}" ] && rm ${fmed_name}
[ -f "${fmed2_name}" ] && rm ${fmed2_name}


3dinfo > /dev/null 2>&1
if [ $? -ne 0 ] && [ ${output} != "timeseries" ]; then
    echo "      Cannot proceed with AFNI."
    exit 1
fi


echo "   - Download data."
start=`date +%s`
curl -sS --fail -u "${hcp_id}:${hcp_pswd}" -o "${fraw}" "${url_full}"
if [ $? -eq 0 ]; then
    end=`date +%s`
    runtime=$((end-start)) 
    echo "      Total: ${runtime}s."
else
    echo "      Fail to get the input data."
    exit 1
fi



echo "   - Voxel-wise preprocessing." 
# Highpass filter, detrend, scaling, and deconvolution, then thresholding voxels
# 1. Detrend remove linear drift: 3dDetrend or part of 3dBandpass
# 2. Highpass filter remove low frequency drift: 3dBandpass [0.01, 0.1]
# 2.5 Downsampling from 2mm to 4mm?
# 3. Deconvolution: 3dDeconvolve -CENSORTR clist -input fname -num_stimts num -stim_file k kfilename 
# 4. Thresholding: fslmaths <input> -cpval <output>
# 3dcalc
#  Same as example #5, but this time create a mask of 8 different values
#    showing all combinations of activations (i.e., not only where        
#    everything is active, but also each stimulus individually, and all   
#    combinations).  The output mask dataset labels voxel values as such: 

#         0 = none active    1 = A only active    2 = B only active       
#         3 = A and B only   4 = C only active    5 = A and C only        
#         6 = B and C only   7 = all A, B, and C active                   

#      3dcalc -a 'func+orig[12]' -b 'func+orig[15]' -c 'func+orig[18]' \
#             -expr 'step(a-4.2)+2*step(b-2.9)+4*step(c-3.1)'          \
#             -prefix mask_8                                              

#    In displaying such a binary-encoded mask in AFNI, you would probably 
#    set the color display to have 8 discrete levels (the '#' menu).      

torig=0
ncensor=10
ntimes=$(echo $(3dinfo -ntimes ${fraw}) )
TR=$(echo $(3dinfo -TR ${fraw}) )

fname="${label}_${sid}.nii.gz"

# Step 1. Remove first 10 time points for all voxels
echo "         - Remove first 10 TRs."
fproc1="${spath}/step1_${fname}"
3dTcat -overwrite -prefix ${fproc1} ${fraw}'[10..$]' > /dev/null 2>&1

echo "         - Despike."
fproc2="${spath}/step1b_${fname}"
3dDespike -overwrite -nomask -quiet -NEW -prefix ${fproc2} ${fproc1} > /dev/null 2>&1


# Step 2&3. Remove linear drift and low frequency drift
fproc="${spath}/${fname}"
3dTproject -overwrite -quiet -polort 2 -bandpass 0.01 99999 -prefix ${fproc} -input ${fproc2} > /dev/null 2>&1


# Step 4. Deconvolution to remove HRF for tfMRI
if [[ ${dl_type} == "tfMRI" ]]; then
    echo "         - Generate stimulus sequence."

    sspath="${spath}/${label}_${sid}"
    if [ ! -d ${sspath} ]; then mkdir ${sspath}; fi

    convolve="3dDeconvolve -nobucket -input ${fproc} -trans -overwrite"

    nstim=$(( ${#filelist[@]} - 1 )) # length of filelist
    cstim="-num_stimts ${nstim}"
    for (( i=0; i<${nstim}; i++ ));  do
        k=$(( i + 1 ))
        file="${filelist[$i]}"

        fev="${sspath}/${file}" 
        url_full="${url}/EVs/${file}.txt"
        curl -sS -f -o "${fev}.txt" -u ${hcp_id}:${hcp_pswd} ${url_full}
        if [ $? -eq 0 ]; then
            Rscript "${repo}/preprocessing/get_stim_sequence.r" "${ntimes}" "${fev}.txt" "${fev}.1D"
            fev_size=$( echo $( ${repo}/preprocessing/get_file_size.sh ${fev}.1D ) )
            if [ ${fev_size} -gt 0 ]; then
                cstim="${cstim} -stim_file ${k} ${fev}.1D -stim_label ${k} ${file}"
            fi
        fi
        # python "${repo}/preprocessing/get_stim_sequence.py" ${ntimes} "${fev}.txt" ${ncensor} ${TR} "${fev}.1D"
    done

    echo "         - Deconvolve."
    fproc3="${spath}/step3_errts_${fname}"
    # convolve="${convolve} ${cstim} -errts ${fproc3} -fitts ${spath}/step3_fitts_${fname}"
    convolve="${convolve} ${cstim} -errts ${fproc3}"
    eval ${convolve} > /dev/null 2>&1

    if [ -f ${fproc3} ]; then
        [ -f ${fproc} ] && rm ${fproc}
        mv ${fproc3} ${fproc}; 
        # fproc="${fproc3}"
    fi
fi


# fproc: "~/Dropbox/data/rfMRI/REST1/${label}_${sid}.nii.gz"
# label = "${dl_type}_${task}_${phase}"




echo "   - Extract ROI statistics."
start=`date +%s`


if [[ "${output}" =~ timeseries ]]; then
  python3 "${repo}/preprocessing/extract_timeseries_264rois.py" "${fproc}"
else
    roilist=($(ls ${roi_input}))
    nroi=${#roilist[@]}

    [ -f "${spath}/${label}_${sid}_median.txt" ] && rm "${spath}/${label}_${sid}_median.txt"
    touch "${spath}/${label}_${sid}_median.txt"

    [ -f "${spath}/${label}_${sid}_mode.txt" ] && rm "${spath}/${label}_${sid}_mode.txt"
    touch "${spath}/${label}_${sid}_mode.txt"

    [ -f "${spath}/${label}_${sid}_mean.txt" ] && rm "${spath}/${label}_${sid}_mean.txt"
    touch "${spath}/${label}_${sid}_mean.txt"
    for roi_fname in ${roilist[@]} # loop through all ROIs
    do
	    maskin="${roi_input}/${roi_fname}"
	    rid=$(echo ${roi_fname} | awk -F'_' '{ print $1 }')
        if [ ! -f ${fproc} ]; then
            echo "Error: file ${fproc} not exist."
            exit 1
        fi
	    3dROIstats -mask ${maskin} -quiet -nzmedian -nzmode -nzmean -nobriklab -nomeanout ${fproc} >> "${spath}/${label}_${sid}_${rid}_temp.txt" # ntime x 2

    
        while read nzmedian nzmode nzmean
        do
            echo -n "${nzmedian}  " >> "${spath}/${label}_${sid}_median.txt"
            echo -n "${nzmode}    " >> "${spath}/${label}_${sid}_mode.txt"
            echo -n "${nzmean}    " >> "${spath}/${label}_${sid}_mean.txt"
        done < "${spath}/${label}_${sid}_${rid}_temp.txt"

        echo "" >> "${spath}/${label}_${sid}_median.txt"
        echo "" >> "${spath}/${label}_${sid}_mode.txt"
        echo "" >> "${spath}/${label}_${sid}_mean.txt"

        rm "${spath}/${label}_${sid}_${rid}_temp.txt"
    done
fi



wait
end=`date +%s`
runtime=$((end-start)) 
echo "      Total: ${runtime}s."


if  ! ${CACHED}; then
    [ -f ${fraw} ] && rm ${fraw}
    [ -f ${fproc} ] && rm ${fproc}
    [ -f ${fproc1} ] && rm ${fproc1}
    [ -f ${fproc2} ] && rm ${fproc2}
    [ -f ${fproc3} ] && rm ${fproc3}
    [ -d ${sspath} ] && rm -rf ${sspath}
    rm *Decon*
fi

wait

## quality check
final="${spath}/${label}_${sid}_${output}.txt"
R -e "data = as.matrix(read.table('${final}'))" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "      Generated data file is corrupted -> deleted."
    rm ${final}
    exit 1
fi


echo ">>>>>>    Done"
echo ""
echo ""
echo ""

exit 0