#!/bin/bash
if [ -f "$1" ]; then
    fmed_size=$(echo "$(du -sh "$1" | awk -F' ' '{print $1}' | sed 's/[^0-9]*//g')" )
else
    fmed_size=0
fi

echo $fmed_size