import os, sys
import numpy as np


def main(ntimes, fin, ncensor=10, TR=0.72, fout=None, trim=False):
    '''
    @onset: numeric starting time, such as 162.47
    @ntimes: total number of time points for the fMRI scans. It can be obtained from `3dInfo -ntimes ${stim_file}`
    '''
    if not os.path.isfile(fin):
        print("Error: file not found.")
        return
    
    ev = np.loadtxt(fin) # rows=stim x cols=[onset (duration) (amplitude)]

    if np.count_nonzero(ev) == 0:
        print("Error: file is empty.")
        return
    
    not_block = len(ev.shape) == 1
    if not_block:
        ev = ev[:, np.newaxis]
        duration = 0
    
    nlen = ntimes
    if trim:
        nlen = ntimes - ncensor
    
    stim_seq = np.zeros(nlen, dtype=int)

    nstim = ev.shape[0]
    for i in range(nstim):
        onset = int(np.round(ev[i,0] / TR)) # Index of starting time, truncated if censored.

        if not not_block:
            duration = np.round(ev[i,1] / TR)
        
        finish = int(min(onset + duration - 1, ntimes - 1))

        if trim:
            onset = int(max(onset - ncensor, 0))
            finish = finish - ncensor
        
        stim_seq[onset:finish] = 1
    
    stim_seq = stim_seq[:, np.newaxis]
    if fout is None:
        fout = fin # overwrite the input file

    np.savetxt(fout, stim_seq, fmt='%d')
    return



if __name__ == "__main__":
    ntimes = int(sys.argv[1])
    fin = str(sys.argv[2])
    if (len(sys.argv) > 3):
        ncensor = int(sys.argv[3])
        
    if (len(sys.argv) > 4):
        TR = float(sys.argv[4])

    if (len(sys.argv) > 5):
        fout = str(sys.argv[5])
        
    main(ntimes, fin, ncensor=ncensor, TR=TR, fout=fout)
