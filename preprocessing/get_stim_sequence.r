get_stim_sequence = function(ntimes, fin, fout, ncensor=10, TR=0.72, trim=FALSE) {
    ntimes = as.integer(ntimes)
    stopifnot(file.exists(fin))

    tryCatch({
        con = file(fin)
        ev = read.table(text=readLines(con, warn=FALSE),header=FALSE, sep="\t")
    }, error = function(e) {

        message("No lines available in input.")
        return(1)
    })


    nlen = ntimes
    if (trim) { nlen = ntimes - as.integer(ncensor) }
    stim_seq = rep(0, nlen)

    nstim = nrow(ev)
    is_event = ncol(ev) == 1

    for (i in 1:nstim) {
        onset = round(as.numeric(ev[i,1]) / as.numeric(TR))
        duration = 0
        if (!is_event) { 
            duration = round(as.numeric(ev[i,2]) / as.numeric(TR))
        }

        finish = round(min(c(onset + duration, nlen)))
        if (trim) {
            onset = round(max(onset - as.integer(ncensor), 1))
            finish = finish - as.integer(ncensor)
        }

        stim_seq[onset:finish] = 1

    }

    stim_seq = as.data.frame(stim_seq)

    write.table(stim_seq, fout, row.names = FALSE, col.names = FALSE)
    return()
}


args = commandArgs(trailingOnly = TRUE)
stopifnot(length(args) >= 2)

if (length(args) == 2) {
    get_stim_sequence(rgs[1], args[2])

} else if (length(args) == 3) {
    get_stim_sequence(args[1], args[2], fout = args[3])

} else if (length(args) == 4) {
    get_stim_sequence(
        as.integer(args[1]), as.character(args[2]), 
        fout = as.character(args[3]),
        ncensor = as.integer(args[4]))

} else if (length(args) == 5)  {
    get_stim_sequence(
        as.integer(args[1]), as.character(args[2]), 
        fout = as.character(args[3]),
        ncensor = as.integer(args[4]),
        TR = as.numeric(args[5]))

} else {
    get_stim_sequence(
        as.integer(args[1]), as.character(args[2]), 
        fout = as.character(args[3]),
        ncensor = as.integer(args[4]),
        TR = as.numeric(args[5]),
        trim = as.logical(args[6]))
}


q()