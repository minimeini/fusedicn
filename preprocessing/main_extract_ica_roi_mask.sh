#!/bin/bash
# paths
set -e


unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     pfix="/home";;
    Darwin*)    pfix="/Users";;
    *)          pfix="/UNKNOWN"
esac

ic_type="tfMRI_ic"

data_path="${pfix}/$(whoami)/data"
ica_input="${data_path}/${ic_type}"
curpath=$(pwd)

case "${ic_type}" in
	"tfMRI_ic")
	tasklist=("EMOTION" "GAMBLING" "LANGUAGE" "MOTOR" "RELATIONAL" "SOCIAL" "WM")

	for task in "${tasklist[@]}"
	do
		fsum="${data_path}/${ic_type}_${task}.nii.gz"
		roout="${data_path}/roi_mask/${ic_type}_${task}"

		if [ ! -d "${roout}" ]
		then
			mkdir "${roout}"
		fi

		rin="${ica_input}/${task}/melodic_IC.nii.gz"
		shopt -s nullglob
		nic=(${ica_input}/${task}/report/IC*prob.png)
		nic=$(echo "${#nic[@]}")
		declare -i nic=$nic
		ndigit=$(echo -n $nic | wc -c)


		declare -i cnt=0
		while [ $cnt -lt $nic ]
		do
			# extract roi file
			idx=$(printf "%0${ndigit}d" $(($cnt+1)))
			fname="${roout}/${idx}.nii.gz"

			if [ ! -f "${fname}" ]
			then
				fslroi "${rin}" "${fname}" $cnt 1 # volume idx starts at 0
			fi

			# create binary mask
			fslmaths "${fname}" -bin "${fname}"

			# remove overlap
			if (( $cnt > 0 ))
			then
				fslmaths "${fname}" -sub "${fsum}" -thr 0 -bin $rout
				fslmaths "${fsum}" -add "${fname}" -bin "${fsum}"
			else
				cp "${fname}" "${fsum}"
			fi

			# seperate ROI info left and right hemispheres

			# finish one loop
			cnt=$(($cnt+1))
			unset idx
			unset fname
		done

		rm ${fsum}

		# create ROI info for each task
		info_output="${data_path}/roi_info/${ic_type}_${task}"
		if [ ! -d "${info_output}" ]
		then
			mkdir "${info_output}"
		fi
		infout="${info_output}/${ic_type}_${task}_MNI_2mm.info"

		cd ${roout}
		shopt -s nullglob
		fall=(*.nii.gz)
		rid=($(echo "${fall[@]%%.*}"))
		printf "%s,,\n" "${rid[@]}" >> ${infout}

		# go to next task
		unset nic
		cd ${curpath}
	done

	;;
	"rfMRI_ic")
	task="REST1_LR"
	;;
	*)
	;;
esac


wait
set +e
