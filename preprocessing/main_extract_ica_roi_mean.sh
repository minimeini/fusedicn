set -e

# paths
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     pfix="/home";;
    Darwin*)    pfix="/Users";;
    *)          pfix="/UNKNOWN";;
esac

read -p "The name of TASK: " task

roi_name="tfMRI_ic_${task}"
data_path="${pfix}/$(whoami)/data"
data_input="${data_path}/hcpdata"
sublist=($(dir ${data_input}))

data_output="${data_path}/fmri_roi_mean/${roi_name}"
if [ ! -d "${data_output}" ]
then
	mkdir "${data_output}"
fi

roi_def="${data_path}/roi_info/${roi_name}/${roi_name}_MNI_2mm.info"
roi_output="${data_path}/roi_mask/${roi_name}"
curpath=$(pwd)

shopt -s nullglob
nic=(${roi_output}/*.nii.gz)
nic=$(echo "${#nic[@]}")
declare -i nic=$nic
ndigit=$(echo -n $nic | wc -c)

fname="tfMRI_${task}_LR.nii.gz"
fname_short=$(echo $fname | grep -o '^[^.]*')
if [ ! -d "${data_output}/${fname_short}" ]
then
	mkdir "${data_output}/${fname_short}"
fi

# extract mean ROI time series
for sid in "${sublist[@]}"
do
	fin="${data_input}/${sid}/${fname}"
	fout="${data_output}/${sid}/${fname_short}"

        if [[ -f ${fin} ]]
	then
		if [ ! -d "${data_output}/${sid}" ]
        	then
                	mkdir "${data_output}/${sid}"
        	fi

        	if [ ! -d "${data_output}/${sid}}/${fname_short}" ]
        	then
                	mkdir "${data_output}/${sid}/${fname_short}"
        	fi

		echo "${sid} - ${roi_name}"
		declare -i cnt=0
		while [ $cnt -lt $nic ]
		do
			idx=$(printf "%0${ndigit}d" $(($cnt+1)))
			rout="${roi_output}/${idx}.nii.gz"
			fslmeants -i ${fin} -o ${fout}/${idx}_temp.txt -m ${rout}
			cnt=$(($cnt+1))
		done

		fout_all="${data_output}/${fname_short}/${fname_short}_${roi_name}_mean_${sid}.txt"
		cd ${data_output}/${sid}/${fname_short}
		paste *_temp.txt | column -s $'\t' -t > ${fout_all}
		cd ${curpath}

		unset cnt
	else
		echo "${sid} - ${roi_name}: file doesn't exist."
	fi
done
