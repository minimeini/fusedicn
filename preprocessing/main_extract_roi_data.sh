#!/bin/bash

# run
# `chmod u+x ./main_extra_voxel_ts.sh`
# in the terminal if you can a Permission denied error msg
# set -e

# Input Folder Structure
# > ~/data/tfMRI_ts/TASK/tfMRI_ts_TASK_LR_${sid}.nii.gz
#
# Available Tool
# > fslmeants -i ${fin} -o ${fout}/${rid}_temp.txt -m ${mask}
# > fslmaths ${fin} -mas ${mask} ${fout}/${rid}.nii.gz
# > 3dmaskdump -noijk -mask ${mask} -o ${fout}/${rid}.txt ${fin}

# paths
unameOut=$(uname -s)
case ${unameOut} in
    Linux*)     pfix=/home;;
    Darwin*)    pfix=/Users;;
    *)          pfix=/UNKNOWN
esac

echo Available dataset: tfMRI rfMRI
read -p "Select dataset: "  dl_type

data_path=${pfix}/$(whoami)/Dropbox/data
tfmri_task=(EMOTION GAMBLING LANGUAGE MOTOR RELATIONAL SOCIAL WM)
rfmri_task=(REST1 REST2)

case ${dl_type} in
	rfMRI)
	echo Available tasks: ${rfmri_task[@]}
	;;
	tfMRI)
	echo Available tasks: ${tfmri_task[@]}
	;;
esac
read -p "Select one of the tasks: " task


roi_name=AAL90
roi_input=${data_path}/roi_mask/${roi_name}
roilist=($(ls ${roi_input}))
nroi=${#roilist[@]}


data_input=${data_path}/${dl_type}_ts

data_output=${data_path}/${dl_type}_roi
if [ ! -d ${data_output} ]
then
	mkdir ${data_output}
fi

data_output=${data_output}/${roi_name}
if [ ! -d ${data_output} ]
then
	mkdir ${data_output}
fi


# extract ROI data
din=${data_input}/${task}
dout=${data_output}/${task}
if [ ! -d ${dout} ]
then
	mkdir ${dout}
fi

# sublist: array of subject
# 	You can create a list manually
# 	e.g. `sublist=('filenameOfSubject1' 'filenameOfSubject2')`
#
#	You can also read all files in a specific folder `din`
# 	`sublist=($(ls ${din}))`
#
# 	`@` means loop through all elements in this array
#
# More Info about for loop: https://www.cyberciti.biz/faq/bash-for-loop/

sublist=($(ls ${din}))
for sub_fname in ${sublist[@]} # loop through all subjects
do
	fin=${din}/${sub_fname}
	sid=$(echo ${sub_fname} | awk -F'_' '{ print $5 }')
	sid=$(echo ${sid} | grep -o -E '[0-9]+')
	dsout=${dout}/${sid}

	if [ ! -d ${dsout} ]
	then
		mkdir ${dsout}
	fi

	echo ${task} - ${sid}
	start=`date +%s`

	# roilist=('roiA' 'roiB')
	# roi_fname='roiA'
	for roi_fname in ${roilist[@]} # loop through all ROIs
	do
		maskin=${roi_input}/${roi_fname}
		rid=$(echo ${roi_fname} | awk -F'_' '{ print $1 }')
		# rid=$(echo ${roi_fname} | grep -o -E '[0-9]+')
		fout=${dsout}/${rid}.txt
		echo ${fout}
		if [ ! -f ${fout} ] # only run if output doesn't exist -> save some time
		then # extrac all voxel time series in a ROI
			3dmaskdump -quiet -noijk -mask ${maskin} -o ${fout} -nozero ${fin}
			# fout: every single row corresponds to a voxel in your mask
		fi
	done

	# rm ${fin} # remove original input file permanently
	end=`date +%s`
	runtime=$((end-start))
	echo     Done in ${runtime} seconds.
done
# set +e
wait
