# paths
set -e
pfix=$(realpath ~)


data_path="${pfix}/Dropbox/data"
rname="aal2_for_SPM12"
fname="ROI_MNI_V5"


if [ ! -d ${data_path} ]; then mkdir ${data_path}; fi
if [ ! -d ${roi_output} ]; then mkdir ${roi_output}; fi

roi_output="${data_path}/roi_mask"
if [ ! -d "${roi_output}/AAL90" ]; then mkdir "${roi_output}/AAL90"; fi

cd ${data_path}
wget -q "http://www.gin.cnrs.fr/wp-content/uploads/${rname}.tar.gz"
tar -xzf "${rname}.tar.gz"
rm "${data_path}/${rname}.tar.gz"


rin="${data_path}/aal/${fname}.nii"
rdef="${data_path}/aal/${fname}.txt"


# general ROI masks
rid=1
while read line
do
	label=$(echo $line | awk '{ print $1 }')
	name=$(echo $line | awk '{ print $2 }')
	thres=$(echo $line | awk '{ print $3 }')

	rout="${roi_output}/AAL90/${rid}_${label}.nii.gz"

	fslmaths ${rin} -bin -thr ${thres} -uthr ${thres} ${rout}

	rid=$((rid + 1))
done < ${rdef}

wait
rm -rf "${data_path}/aal"

set +e