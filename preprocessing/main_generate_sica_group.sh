set -e

# setting: path
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     pfix="/home";;
    Darwin*)    pfix="/Users";;
    *)          pfix="/UNKNOWN"
esac
echo ${machine}

savepath="${pfix}/$(whoami)/data/groupsica/melodic"
tasklist=("rest" "lang")

if [ ! -d "${savepath}" ]
then
	mkdir "${savepath}"
fi


# group ICA based on tfMRI

for task in "${tasklist[@]}"
do
(	spath="${savepath}/${task}"
	if [ ! -d "${spath}" ]
	then
		mkdir "${spath}"
	fi

	flist="../info/${task}_filelist.txt"
	flist=$( cat ${flist} )
	melodic -i ${flist} -o "${savepath}/${task}" -d 18 --tr=0.72 --nobet \
		--bgimage="${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz" \
		-m "${FSLDIR}/data/standard/MNI152_T1_2mm_brain_mask.nii.gz" \
		-a concat --report --Oall ) &
done


# Finish
set +e
wait