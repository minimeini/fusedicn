import sys,os,math
import numpy as np


def get_gitpath(debug=False):
	if debug:
		gitpath = os.getenv("HOME") + '/respository/fusedicn'
	else:
		curpath = os.path.abspath(os.path.dirname(sys.argv[0]))
		gitpath = ('/').join(curpath.split(sep='/')[:-1])
	return gitpath


gitpath = get_gitpath(debug=False)
sys.path.insert(1,gitpath+'/utils')
import utils


class TaskEvents(utils.ProjectInfo):
	'''
	Class to store the event(stimulus) specific information.


	>>> Public Variables

	- str_labels: nevent x 1
	- start_time: nevent x 1
	- parent_dir:

	>>> Public Methods

	- print_table
	- calc_end_dur_time
	- read_raw
	- select
	- get_unq_labels
	- get_int_labels
	- generate_sequence
	- trim_sequence
	
	'''

	# file information
	def __init__(self, task=None, parent_dir=None, phase='LR', dir='/Dropbox/data/'):
		utils.ProjectInfo.__init__(
			self, roi_name="AAL90", data_type="tfMRI", 
			phase=phase, dir = dir)
		
		self.phase = str(phase)
		self.task = task
		if task is not None:
			self.ntime = self.task_ntime(self.task)
		else:
			self.ntime = None

		str_labels = None
		start_time = None
		sequence = []
		print(parent_dir)
		self.read_raw(parent_dir=parent_dir)
		self.dloc = 0
		return

	def print_table(self):
		try:
			data = np.hstack((self.str_labels[:,np.newaxis], 
				self.start_time[:,np.newaxis]))
		except:
			print("Nothing to be printed.")

		return

	@staticmethod
	def calc_end_dur_time(start_time, ntime):
		start_time = np.asarray(start_time)
		end_time = np.diff(start_time) - 1 + start_time[:-1]
		end_time = np.append(end_time, ntime-1)
		dur_time = end_time - start_time + 1
		return end_time, dur_time

	def read_raw(self, parent_dir=None):
		'''
		Load all events for one subject in one task.


		>>> REQUIRED INPUT

		- parent_dir
		- filelist


		>>> OUTPUT
	
		- data: nevent x 2, cols = str_label | start_time

		'''
		self.empty = True

		if parent_dir is not None:
			self.parent_dir = parent_dir
		if self.parent_dir is None:
			return
		self.filelist = os.listdir(self.parent_dir)

		data = tuple()
		for file in self.filelist:
			label = file.split('_')[4:]
			label[-1] = label[-1].split('.')[0]
			if len(label) > 1:
				label = '-'.join(label)
			
			try:
				# print(self.parent_dir+'/'+file)
				tmp = np.loadtxt(self.parent_dir+'/'+file)
				tmp = tmp[:,0].astype(float)
				tmp = np.vstack((np.tile(label,tmp.shape[0]),tmp)).T
				data = data + (tmp,)
			except IndexError:
				print('> No record: skip.')

		try:
			data = np.vstack(data)
			self.str_labels = data[:,0]
			self.start_time = data[:,1].astype(float)

			# remove duplicates
			idx = np.unique(self.start_time, return_index=True)[1]
			self.select(idx)
			self.empty = False
		except ValueError:
			print('No any record for this subject.')


	def select(self, idx_keep):
		if self.start_time is not None:
			self.start_time = self.start_time[idx_keep]
		if self.str_labels is not None:
			self.str_labels = self.str_labels[idx_keep]
		return

	@staticmethod
	def get_unq_labels(str_labels):
		unq_index = np.unique(str_labels, return_index=True)[1] 
		# sorted unique values, starting from 1

		unq_labels = np.asarray([str_labels[idx] for idx in sorted(unq_index)])
		return unq_labels

	@staticmethod
	def get_int_labels(str_labels):
		unq_labels = TaskEvents.get_unq_labels(str_labels)
		int_labels = np.asarray([np.where(unq_labels==label)[0][0] for label in str_labels])
		return int_labels


	def generate_sequence(self, remove_duplicate=True, time_to_point=True, 
		pad_start=True, trim_end=True, concate_small=True):
		'''
		Clean the event information:
		1. convert start time to data point
		2. pad the beginning if there is a long resting period
		3. remove events with start time > (ntime-1)
		4. concatenate events with duration < `info.minlen`(default=2) pnts
		5. add end time (point)

		'''
		if self.empty:
			return

		# convert start time to data point
		if time_to_point:
			self.start_time = np.floor(self.start_time / self.TR())
			if self.start_time[0] <= self.minlen:
				self.start_time[0] = 0.0

		# pad the beginning if there is a long resting period
		if pad_start and self.start_time[0] > 0:
			self.str_labels = np.append(self.pad_label, self.str_labels)
			self.start_time = np.append(0.0, self.start_time)
		
		# remove events with start time > (ntime-1)
		if trim_end:
			idx_keep = np.where(self.start_time<(self.ntime-1))[0]
			self.select(idx_keep)

		# concatenate events with duration < `info.minlen`(default=2) pnts
		if concate_small:
			dur_time = TaskEvents.calc_end_dur_time(self.start_time,self.ntime)[1]
			idx_keep = np.where(dur_time>=self.minlen)[0]
			self.select(idx_keep)

		dur_time = TaskEvents.calc_end_dur_time(self.start_time,self.ntime)[1]
		# add integer labels
		int_labels = TaskEvents.get_int_labels(self.str_labels)

		self.sequence = []
		for label, dur in zip(int_labels, dur_time):
			self.sequence.extend(np.tile(label,int(dur)).tolist())
		self.sequence = np.asarray(self.sequence)

		return

	def trim_sequence(self):
		if self.empty:
			return
		if self.str_labels[0] == self.pad_label:
			end_time = TaskEvents.calc_end_dur_time(self.start_time, self.ntime)[0]
			self.dloc = max(end_time[0]+1, self.ndel)
		else:
			self.dloc = self.ndel

		assert(self.dloc is not None)
		self.dloc = int(self.dloc)
		self.sequence = self.sequence[self.dloc:]
		return


# def main(trim=True, save=True, datapath=None, 
# 	savepath=None, task_labels=None):
# 	'''
# 	Generate stimuli(events) sequence for all subjects within one task, 
# 	can process multiple task in one go.


# 	>>> OPTIONAL INPUT (kwargs: key=value)

# 	- datapath: str, default = 'userpath/data/tfMRI_ev'
# 	- savepath: str, default = 'userpath/data/tfMRI_ev_proc'
# 	- variables in class `ProjectInfo`


# 	>>> OUTPUT

# 	- AllStamps: a list of instance of class `EventInfo`.

# 	'''
# 	if task_labels is None:
# 		task_labels = utils.ProjectInfo.get_labels("tfMRI")
	
# 	for task in task_labels:
# 		stamps = utils.EventInfo(task)
# 		filelist = stamps.all_evlist()
		
# 		for flist in filelist: # loop through each subject
# 			sid = flist[0].split(sep='_')[4]
# 			parent_dir = utils.TaskLoader.update_abs_paths(stamps,sid).ev_raw
# 			events = TaskEvents(parent_dir=parent_dir, task=task)
# 			if not events.empty:
# 				events.generate_sequence()
# 				if trim:
# 					events.trim_sequence()

# 				stamps.add(events.sequence, events.dloc, sid)

# 		if save:
# 			stamps.save()

# 	return

def main(sid, trim=True, save=True, datapath=None, 
	savepath=None, task="LANGUAGE", phase="LR", dir='/Dropbox/data/'):
	'''
	Generate stimuli(events) sequence for all subjects within one task, 
	can process multiple task in one go.


	>>> OPTIONAL INPUT (kwargs: key=value)

	- datapath: str, default = 'userpath/data/tfMRI_ev'
	- savepath: str, default = 'userpath/data/tfMRI_ev_proc'
	- variables in class `ProjectInfo`


	>>> OUTPUT

	- AllStamps: a list of instance of class `EventInfo`.

	'''
	stamps = utils.EventInfo(
		task=task, data_type="tfMRI", roi_name="AAL90", phase=phase, dir=dir)
	parent_dir = utils.TaskLoader.update_abs_paths(stamps,sid).ev_raw
	events = TaskEvents(parent_dir=parent_dir, task=task, phase=phase, dir=dir)

	if not events.empty:
		events.generate_sequence()
		if trim:
			events.trim_sequence()

		stamps.add(events.sequence, events.dloc, sid)
		

	if save:
		stamps.save()

	return


# if __name__ == "__main__":
# 	print("--- Initialization Start ---")
# 	print(">>> Provide task type - multitask use single space as a delimiter: ")
# 	labels = str(input())
# 	labels = labels.upper().split(sep=' ')
# 	print("--- Initialization Done ---")

# 	main(trim=True, save=True, task_labels=labels)




if __name__ == "__main__":
	sid = sys.argv[1]
	task = sys.argv[2]
	phase = sys.argv[3]
	dir = sys.argv[4]
	main(sid, trim = True, save = True, 
	  datapath = None, savepath = None, 
	  task = task, phase = phase, dir = dir)