read_file = function(fname) {
    con = file(fname, open = 'r')
    dat = readLines(con)
    dat2 = vector("list", length = length(dat))

    for (i in 1:length(dat)) {
        row = strsplit(dat[i], "\\s+")
        row = c(as.numeric(row[[1]]))
        dat2[[i]] = row
    }
}