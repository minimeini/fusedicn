#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

print(">>>>>>>>>>>>>>   Initializing R environment")

sid = "101107"
if (length(args) >= 1) {
    sid = as.character(args[1])
}

task = "REST1"
if (length(args) >= 2) {
    task = as.character(args[2])
}

nfactor <- 20
if (length(args) >= 3) {
    nf = as.integer(args[3])
    if (! is.null(nf) && nf > 0) {
        nfactor = nf
    } else if (is.null(nf)) {
        print("Missing number of factors argument, use default value nfactor = 20.")
    } else {
        print("Invalid number of factors argument, use default value nfactor = 20.")
    }
}

ftype = "timeseries"
if (length(args) >= 4) {
    ftype = as.character(args[4])
}


nsample <- 5000
if (length(args) >= 5) {
    nsample = as.integer((args[5]))
}

nthin <- 2
if (length(args) >= 6) {
    nthin = as.integer(args[6])
}

nburnin <- 10000
if (length(args) >= 7) {
    nburnin = as.integer((args[7]))
}


print(
paste(
    paste(
        "data =", sid, task, sep = " "
    ), 
    paste(
        "nfactor =", nfactor, sep = " "
    ),
    paste(
        "mcmc =", nsample, nthin, nburnin, sep = ":"
    ),
    sep = "; "
)
)

if (Sys.getenv("USER") == "meinitang") {
    upath = normalizePath("~/Dropbox")
    small_memory = TRUE
} else {
    upath <- normalizePath("~")
    small_memory = FALSE
}

dpath = file.path(upath, "data")
repo = file.path(upath, "repository", "bicnet-new")
Rcpp::sourceCpp(file.path(repo, "evaluation.cpp"))

opath = file.path(dpath, "mcmc_fsv")
if (!dir.exists(opath)) {
    print(paste0("Try to create folder ", opath))
    tryCatch({
        dir.create(opath, showWarnings = FALSE)
    }, error = function(e){
        stop("Fail to create this folder.")
    })
}
print(paste0("Output folder: ", opath))

tryCatch({
    library("factorstochvol")
}, error = function(e) {
    stop("Fail to load R package factorstochvol.")
})



# plot_mode <- "plot"

if (task == "REST1" | task == "REST2") {
    modality = "rfMRI"
} else {
    modality = "tfMRI"
}

# task.all <- c("REST1", "REST2", "LANGUAGE", "GAMBLING", "EMOTION", "RELATIONAL", "SOCIAL", "MOTOR", "WM")
# mod.all <- c(rep("rfMRI", 2), rep("tfMRI", 7))
phase <- "LR"

# sname = file.path(dpath, modality, task, phase)
pname <- file.path(dpath, modality, task)
fname <- paste(modality, task, phase, sid, paste0(ftype, ".txt"), sep = "_")
sname = file.path(pname, fname)
print(sname)
stopifnot(file.exists(sname))

if (ftype == "timeseries") {
    nroi <- 264
} else {
    nroi = 90
}

print(">>>>>>>>>>>>>>   Run FSV algorithm")
fname_out <- paste0(paste(paste0("mcmc_f", nfactor), sid, task, ftype, sep = "_"), ".rdata")
sname_out = file.path(dpath, "mcmc_fsv", fname_out)
if (file.exists(sname_out)) {
    stop(paste0("File ", sname_out, " already exists."))
}

# tryCatch(
#     {
tryCatch({
    data <- as.matrix(read.table(sname, header = FALSE)) # ntime x nroi
}, error = function(e){stop(paste0("Error: cannot open file ", sname))})
        
        if (ftype != "timeseries") {
            data = t(data) # ntime x nroi
            # data = apply(data, 2, function(col){(col - mean(col)) / sd(col)}) # standardization
        }
        output <- factorstochvol::fsvsample(data, factors = nfactor, draw = nsample, thin = nthin, burnin = nburnin, restrict = "upper", quiet = TRUE)

        metrics = eval_info_criteria(t(data), output)
        Lambda = apply(output$facload, c(1,2), mean) # nroi x nfac
        Zmap = apply(output$facload, c(1,2), function(lam){
            nonzero = length(unique(sign(quantile(lam, c(0.025, 0.975))))) == 1
            lam = nonzero
        })
        Zmap0 = !output$config$restrict
        Zmap = Zmap * Zmap0 # nroi x nfac
        fac = output$runningstore$fac

        # if (small_memory) {
            var_list = c("fac", "Lambda", "Zmap", "metrics", "data")
        # } else {
        #     var_list = c("Lambda", "Zmap", "metrics", "output", "data")
        # }
        
        var_list = var_list[sapply(var_list, exists)]
        save(list = var_list, file = sname_out)
#     },
#     error = function(e) {
#         stop(paste0("Error - skipped ", task, " for subject ", sid))
#     }
# )


q()