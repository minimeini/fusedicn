#!/bin/bash
#SBATCH -job-name=hcp_preproc
#SBATCH --ntask=1
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=32
#SBATCH --output=log_%j.txt
#SBATCH --mail-user=mtang41@ucsc.edu
#SBATCH --mail-type=ALL
#SBATCH --mem-per-cpu=8G

## run the application:
isibex=${1:-1}
nburnin=${2:-10000}
niter=${3:-10000}
nthin=${4:-1}
kest=${5:-6}
cont=${6:-0}
prevjobid=${7:-00000000}
mcmcfilename=${8:-"mcmc_nsub_200_K18_20Nov13.rds"}

NWORKER=48


if [[ $isibex -eq 1 ]]
then
	export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
	module load gcc/6.4.0
	module load R/3.6.0/gnu-6.4.0
	backpath="/tmp/output"
	datapath="/ibex/scratch/tangm0a/data/EEG/Trauma.csv"
else
	backpath="./output"
	datapath="~/tangm0a/data/EEG/Trauma.csv"
fi

Rscript ../bicnet/rutils/inst.R

if [[ $cont -eq 0 ]]
then
	R -e "source('main_eeg_data.R');main_eeg_data(Kest=${kest},nburnin=${nburnin},niter=${niter},nthin=${nthin},back_path=${backpath},data_path=${datapath});q()"
fi

if [[ $isibex -eq 1 ]]
then
	rsync -a /tmp/output tangm0a@ilogin.ibex.kaust.edu.sa:/ibex/scratch/tangm0a/repository/fusedicn/output/EEG/${SLURM_JOB_ID}
fi
echo "Done."
