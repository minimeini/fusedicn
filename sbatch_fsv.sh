#!/bin/bash
#SBATCH --job-name=hcp_fsv        # Job name
#SBATCH --output=out_%x_%j.txt           # Standard output file
#SBATCH --error=err_%x_%j.txt             # Standard error file
#SBATCH --nodes=1                     # Number of CPU cores
#SBATCH --ntasks=1                    # Run on a single CPU
#SBATCH --mem=8G
#SBATCH --time=12:00:00                # Maximum runtime (D-HH:MM:SS)
#SBATCH --mail-type=ALL               # Send email at job completion
#SBATCH --mail-user=mtang41@ucsc.edu    # Email address for notifications

# set +e

phase="LR"

task="LANGUAGE"
nfactor=10
revert=0 # 0 or 1
output="timeseries"

while getopts ":t:f:o:r:" opt; do
  case $opt in
    t)
    task=$OPTARG
    ;;
    f)
    nfactor=$OPTARG
    ;;
    o)
    output=$OPTARG # timeseries, mean, median, mode
    ;;
    r)
    revert=$OPTARG
    ;;
    *)
    echo "Invalid argument."
    ;;
  esac
done


if [[ "${USER}" == "mtang41" ]]; then
    repo="${HOME}/repository/fusedicn"
    datapath="${HOME}/data"

    echo ">>>>>>>>>>>>>>   Setup Slurm environment"
    bash "${repo}/setup_slurm_env.sh"
else
    repo="${HOME}/Dropbox/Repository/fusedicn"
    datapath="${HOME}/Dropbox/data"
fi

if [[ "${revert}" -eq 1 ]]; then
    sublist="${repo}/info/hcp_subject_info_rev.txt"
else
    sublist="${repo}/info/hcp_subject_info.txt"
fi


hbadd="mtang41@hbfeeder.ucsc.edu"
hcp_id="welkin31"
hcp_pswd="hetcube31"

echo ">>>>>>>>>>>>>>   Start MCMC"



if [[ "${task}" =~ "REST" ]]; then 
    modality="rfMRI"
else
    modality="tfMRI"
fi


while read sid status; do
    echo "${task} - Subject ${sid}"
    bash "${repo}/get_mcmc_data.sh" -s "${sid}" -t "${task}" -p "${phase}" -r "${repo}" -d "${datapath}" -f "${nfactor}" -o "${output}"
done < $sublist

echo -e ">>>>>>>>>>>>>>   Completed.\n"
# set -e
exit 0