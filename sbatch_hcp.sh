#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=16
#SBATCH --output=log_%j.txt
#SBATCH --mail-user=mtang41@ucsc.edu
#SBATCH --mail-type=ALL
#SBATCH --mem-per-cpu=4G

set +e

NWORKER=6
module load miniconda3.9
# module load R
# module load python-3.9.7
# module unload numpy
conda activate hbenv

task=$1
phase=$2
output=$3

# pfix=$(realpath ~)
if [[ "${USER}" == "mtang41" ]]; then
    repo="${HOME}/repository/fusedicn"
    datapath="${HOME}/data"
else
    repo="${HOME}/Dropbox/Repository/fusedicn"
    datapath="${HOME}/data"
fi

sublist="${repo}/info/hcp_subject_info.txt"


hcp_id="welkin31"
hcp_pswd="hetcube31"
# Credentials of [Connectomedb](https://db.humanconnectome.org/app/template/Login.vm).



while read sid status; do
    nactive=$(ps aux | grep -cw bash)
    if [[ "$nactive" -ge "$NWORKER" ]]; then
        wait
    fi
	bash "${repo}/preprocessing/get_data.sh" -s ${sid} -t "${task}" -p "${phase}" -u "${hcp_id}:${hcp_pswd}" -r "${repo}" -d "${datapath}" -o ${output}
done < $sublist


wait

set -e
exit 0