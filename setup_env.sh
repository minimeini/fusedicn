# ENV=hb

module load miniconda3
conda create -n fsv r-essentials r-base conda-libmamba-solver -q -y -c r -c conda-forge
# conda create -n hb conda-libmamba-solver

conda install -y python pip numpy matplotlib statsmodels pandas psutil joblib pathos scikit-learn freetype --solver libmamba

conda install -y openmotif openmotif-dev gsl xorg-libx11 xorg-libxt xorg-libxext xorg-libxi xorg-libxft xorg-libxcb xorg-libxpm xorg-libxmu libglu glib expat freeglut tcsh ffmpeg netpbm lapack armadillo libgcc-ng llvm-openmp nilearn -c conda-forge --solver libmamba

conda install -y r-base r-essentials r-rcpp r-rcpparmadillo r-afex r-phia r-snow r-nlme r-paran r-brms r-shiny r-shinydashboard r-plotly r-colourpicker r-data.table r-gplots r-rcolorbrewer r-psych r-ggplot2 r-ggridges r-dplyr r-tidyr r-scales r-corrplot r-metafor -c conda-forge --solver libmamba

conda install -y -c conda-forge nipy nilearn nibabel

conda install -n fsv r-factorstochvol -q -y -c r -c conda-forge
RESULT=$?
if [ $RESULT -eq 0 ]; then
    echo "Success"
else
    echo "Try to install factorstochvol locally."
    wget https://anaconda.org/r/r-factorstochvol/1.0.6/download/linux-64/r-factorstochvol-1.0.6-r43h884c59f_0.tar.bz2
    conda install -n fsv --offline "r-factorstochvol-1.0.6-r43h884c59f_0.tar.bz2" -q -y
    rm "r-factorstochvol-1.0.6-r43h884c59f_0.tar.bz2"
fi


cd ~
curl -O https://afni.nimh.nih.gov/pub/dist/bin/misc/@update.afni.binaries
tcsh @update.afni.binaries -package linux_centos_7_64 -do_extras

