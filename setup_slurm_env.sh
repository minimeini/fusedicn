#!/bin/bash

# echo ">>>>>>>>>>>>>>   Setup Slurm environment"

# module load miniconda3.9
# module load R/R-4.3.1 
# module load python-3.9.7
# module unload numpy

# conda init bash

source ~/.bash_profile


if conda info --envs | grep -q fsv; then
    echo "Use existing Conda environment fsv."
else
    echo "Try to create new Conda environment fsv."
    conda create -n fsv r-essentials r-base r-factorstochvol r-rcpparmadillo -q -y -c r -c conda-forge

    RESULT=$?
    if [ $RESULT -eq 0 ]; then
        echo "Success"
    else
        echo "Try to install factorstochvol locally."
        conda create -n fsv r-essentials r-base -q -y -c r -c conda-forge
        conda install -n fsv --offline "${repo}/linux-64_r-factorstochvol-1.0.6-r43h884c59f_0.tar.bz2" -q -y
    fi

fi

conda activate fsv