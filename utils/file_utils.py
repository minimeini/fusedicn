import os,sys,pickle,psutil,gc,math,time
import numpy as np
import multiprocessing
from joblib import Parallel, delayed
from timeit import default_timer as timer


def get_gitpath(debug=False):
    if debug:
        gitpath = os.getenv("HOME") + 'Dropbox/respository/base-connectivity'
    else:
        curpath = os.path.abspath(os.path.dirname(sys.argv[0]))
        gitpath = ('/').join(curpath.split(sep='/')[:-1])
    return gitpath


gitpath = get_gitpath(debug=False)
sys.path.insert(1,gitpath+'/utils')
import prep_utils as prep


def get_datapath(debug=False):
    datapath = os.getenv("HOME") + '/Dropbox/data'
    return datapath


def get_sublist_intersect(wd_rest, wd_task, nsub=None):
    frest = [f for f in os.listdir(wd_rest) if 'csv' in f]
    ftask = [f for f in os.listdir(wd_task) if 'csv' in f]

    sublist = set([f.split('_')[-1].split('.')[0] for f in frest])
    sublist = sublist.intersection(set([f.split('_')[-1].split('.')[0] for f in ftask]))
    sublist = list(sublist)
    if nsub is not None:
        sublist = sublist[:min(nsub,len(sublist))]
    return sublist


def load_rest_task(wd_rest, wd_task, nload=None, detrend=True, num_cpus=None):
    '''
    '''
    if num_cpus is None:
        num_cpus = int(psutil.cpu_count(logical=False)/2)

    sublist = get_sublist_intersect(wd_rest, wd_task)
    frest = [f for f in os.listdir(wd_rest) if 'csv' in f]
    ftask = [f for f in os.listdir(wd_task) if 'csv' in f]
    frest = [f for f in frest if f.split('_')[-1].split('.')[0] in sublist]
    ftask = [f for f in ftask if f.split('_')[-1].split('.')[0] in sublist]

    data_rest = []
    data_task = []
    if nload is None:
        nload = len(sublist)
    else:
        nload = min(len(sublist), nload)

    for i in range(nload):
        f_rest = wd_rest+'/'+frest[i]
        f_task = wd_task+'/'+ftask[i]
        data_rest.append(np.genfromtxt(f_rest, delimiter=',').T)
        data_task.append(np.genfromtxt(f_task, delimiter=',').T)

    if detrend:
        data_rest = Parallel(n_jobs=num_cpus)(delayed(prep.detrend)(d) for d in data_rest)
        gc.collect()
        data_task = Parallel(n_jobs=num_cpus)(delayed(prep.detrend)(d) for d in data_task)
        gc.collect()

    data_rest = np.concatenate([d[np.newaxis,:,:] for d in data_rest], axis=0).squeeze()
    data_task = np.concatenate([d[np.newaxis,:,:] for d in data_task], axis=0).squeeze()
    return data_rest, data_task


def load_set_data(file, detrend=True):
    data = np.loadtxt(file)
    ntime = 215
    nsub = int(data.shape[0]/ntime)
    nroi = data.shape[1]
    data = np.reshape(data, (nsub, ntime, nroi))

    if detrend:
        data = prep.detrend(data)

    stimuli = np.hstack([np.tile('rest',60),np.tile('task',77),
        np.tile('rest',78)])
    data_rest = prep.detrend(data[:,stimuli=='rest',:])
    data_task = prep.detrend(data[:,stimuli=='task',:]) 
    return data_rest, data_task


def load_hcp_stimuli(roi_name="AAL90", wd_rest=None,rest_type="REST1",wd_task=None,task_type="LANGUAGE",nsub=None,debug=False):
    datapath = get_datapath(debug=debug)
    if wd_rest is None:
        wd_rest =  datapath + "/rfMRI_roi_mean/" + roi_name + '/'+ rest_type
    if wd_task is None:
        wd_task =  datapath + "/tfMRI_roi_mean/" + roi_name + '/' + task_type

    wd_ev = datapath + "/tfMRI_ev_proc/tfMRI_ev_proc_" + task_type
    sublist = get_sublist_intersect(wd_rest, wd_task, nsub=nsub)
    subidx = np.genfromtxt(wd_ev+".csv",dtype=None,delimiter=',')

    selector = [si in [int(s) for s in sublist] for si in subidx[:,0]]
    subidx = subidx[selector,:]
    assert(all(subidx[:,1]==0))

    subev = pickle.load(open(wd_ev+".ev", "rb"))
    subev = [subev[i] for i in range(len(subev)) if selector[i]]
    return subev


def load_bayes_result():
    return

