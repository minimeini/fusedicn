#!/bin/bash
K=${1:-1}
jobid=${2:-10000}

src="tangm0a@ilogin.ibex.kaust.edu.sa:/ibex/scratch/tangm0a/repository/fusedicn/output"
desc="${HOME}/Dropbox/repository/fusedicn/output/RankSelection/Kest${K}"

if [ ! -d "${desc}" ]
then
	mkdir -p "${desc}"
fi

rsync ${src}/${jobid}/output/ModLogLike.bin ${desc}/ModLogLike.bin
rsync ${src}/${jobid}/output/ModLogLike.desc ${desc}/ModLogLike.desc
rsync ${src}/${jobid}/output/Pi.bin ${desc}/Pi.bin
rsync ${src}/${jobid}/output/Pi.desc ${desc}/Pi.desc