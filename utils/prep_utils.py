import os,sys,time,pickle,math
import numpy as np
import pandas as pd
import itertools as it
from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression as lm
from statsmodels.tsa.stattools import adfuller


def detrend(data, normalize=False):
	'''
	Regress out the deterministic mean and linear trend by ROI-wise regression.
	
	>>> INPUT
	
	- data: 3darray, nsub x ntime x nroi; 2darray, ntime x nroi; 1darray, ntime

	>>> OUTPUT
	
	- data: the same dimension as the input
	''' 

	# boardcast data to 3darray
	orig_shape = data.shape
	if len(orig_shape)==1: # ntime
		data = data[np.newaxis,:,np.newaxis]
	elif len(orig_shape) == 2: # ntime x nroi
		data = data[np.newaxis,:,:]
	nsub, ntime, nroi = data.shape

	# design matrix
	X = np.arange(1,ntime+1)[:,np.newaxis]
	X[:,0] = X[:,0] - np.mean(X[:,0])
	if normalize:
		X[:,0] = X[:,0] / np.std(X[:,0].squeeze())

	# roiwise regression
	for s in range(nsub):
		for r in range(nroi):
			Y = data[s,:,r]
			mod = lm(fit_intercept=True,normalize=normalize).fit(X,Y)
			data[s,:,r] = Y - mod.predict(X)

	# output has the same dimension as the input
	data = data.squeeze()
	try:
		assert(data.shape == orig_shape)
	except AssertionError:
		print(orig_shape)
		print(data.shape)
		raise AssertionError
	
	return data


def find_integration_order(data, pval_bound=0.05, return_ndarray=True):
	'''
	Use ADF test to find the order of integration for each roi. The null 
	hypothesis is there is a unit root, the alternative is not.

	>>> INPUT

	- data: 3darray nsub x ntime x nroi, or a list of 2darray ntime x nroi, 
		or a list of 1darray ntime
	- pval_bound: default=0.05

	>>> OUTPUT

	- ords: a list of 1darray nroi

	'''
	if type(data)==np.ndarray:
		data = data.tolist()
		for s in range(len(data)):
			data[s] = np.asarray(data[s])
	
	assert(type(data)==list)
	nsub = len(data)
	ords = []
	for s in range(nsub):
		if len(data[s].shape)<2:
			data[s] = data[s][:,np.newaxis]
		
		ntime,nroi = data[s].shape
		tmp = np.zeros((nroi))
		for r in range(nroi):
			pval = 1
			ndiff = -1
			while pval >= pval_bound: # diff if couldn't reject the null
				ndiff = ndiff + 1
				pval = adfuller(np.diff(data[s][:,r], n=ndiff))[1]

			tmp[r] = ndiff
		
		ords.append(tmp)

	if return_ndarray:
		ords = np.hstack(ords)
	
	return ords


def pca_everything(data, nfactor=5, return_ndarray=True):
	'''
	Perform PCA on data and return factors, factor loadings, and residuals.

	>>> INPUT

	- data: (nsub x) ntime x nroi array
	- n_components
	- return_ndarray

	>>> OUTPUT

	- f: (nsub x) ntime x nfactor
	- U: (nsub x) nroi x nfactor
	- epsilon: (nsub x) ntime x nroi

	'''
	if len(data.shape) == 2: # ntime x nroi
		data = data[np.newaxis,:,:]
	nsub = data.shape[0]

	decomp = PCA(n_components=nfactor, svd_solver = 'full', whiten=False)
	f = []; U = []; epsilon = []
	
	for s in range(nsub):
		Y = data[s,:,:].squeeze() # ntime x nroi
		est = decomp.fit(Y); 
		U.append(est.components_.T) # nroi x nfactor
		f.append(Y @ U[s]) # f_all: ntime x nfactor
		epsilon.append(Y - f[s] @ U[s].T) # ntime x nroi

	if return_ndarray:
		f = np.concatenate([ff[np.newaxis,:,:] for ff in f], axis=0).squeeze()
		U = np.concatenate([uu[np.newaxis,:,:] for uu in U], axis=0).squeeze()
		epsilon = np.concatenate([ee[np.newaxis,:,:] for ee in epsilon], axis=0).squeeze()

	return f, U, epsilon


def sliding_window(ts_data, width=15, stepsize=1):
	'''
	Returns a sliding window (of width n) over data from the iterable.
	s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...

	>>> INPUT

	- ts_data: numpy 2darray, nroi x ntime.
	- wlen: int
	- shift: int

	>>> OUTPUT

	- win_data: list of numpy 2darray (nroi x width)

	'''
	assert(width>0)
	nroi = ts_data.shape[0]
	npad = math.floor(width/2)

	pad_data = np.hstack((np.zeros((nroi,npad)),
						  ts_data,np.zeros((nroi,npad))))
	nt = pad_data.shape[1]

	start = np.arange(0, nt-width+1, stepsize)
	nwin = start.shape[0]

	win_data = []
	for nw in range(nwin):
		win_data.append(pad_data[:,start[nw]:start[nw]+width])

	return win_data


def calc_conn(win_data):
	'''
	Calculate pearson's covariance.

	>>> REQUIRED INPUT

	- win_data: a list of numpy 2darray, nroi x ntime

	>>> OUTPUT

	- conn: numpy 2darray, nwin x nroi x nroi

	'''
	conn = []
	for data in win_data:
		conn.append(np.cov(data)[:,:,np.newaxis])
	
	conn = np.concatenate(conn, axis=2)  
	return conn



