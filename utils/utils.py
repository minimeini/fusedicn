'''
Utils module.


>>> FUNCTIONS

- get_hrf


>>> CLASSES

- PATH
- ABSPATH
- ProjectInfo
- TaskLoader
- EventInfo


>>> MISC

- For debugging in ipython, you might want to enable autoreload: 
	`%load_ext autoreload`
	`%autoreload 2`

'''


import sys,os,math
import numpy as np
import pickle

try:
	import nipy.modalities.fmri.hrf as hrf

	def get_hrf(TR, peak_delay=6, under_delay=16, peak_disp=1, 
		under_disp=1, p_u_ratio=6, onset=0, kernel_len=32, res=16):
		'''
		SPM default haemodynamic response function.


		>>> REQUIRED INPUT

		- TR: scan repeat time


		>>> OPTIONAL INPUT

		The unit of time is seconds.

		- peak_delay: float, default=6, delay of peak.
		- under_delay: float, default=16, delay of undershoot.
		- peak_disp: float, default=1, width of peak.
		- under_disp: float, default=1, width of undershoot.
		- p_u_ratio: float, default=6, peak to undershoot ratio.
		- onset: float, default=0.
		- kernel_len: float, default=32, length of kernel.
		- res: float, default=16, microtime resolution.


		>>> OUTPUT

		- hrf: float array, size = 1 x floor(kernel_len/TR)

		'''
		dt = TR/res
		u = np.arange(0,math.ceil(kernel_len/dt)+1,1) - onset/dt
		hrf_resp = hrf.spm_hrf_compat(u, peak_delay=peak_delay/dt, 
			under_delay=under_delay/dt, peak_disp=peak_disp/dt,
			under_disp=under_disp/dt, p_u_ratio=p_u_ratio, 
			normalize=True)
		hrf_idx = np.arange(0,math.floor(kernel_len/TR)+1,1) * res
		hrf_resp = hrf_resp[hrf_idx]
		hrf_resp = hrf_resp / sum(hrf_resp)
		return hrf_resp
except ModuleNotFoundError:
	pass



def get_event_info(task):
	if task == "EMOTION":
		ev = ["fear", "neut"]
		# ev = ["fear"]
	elif task == "GAMBLING":
		ev = ["loss", "win", "win_event", "loss_event", "neut_event"]
		# ev = ["loss", "win", "win_event", "loss_event"]
	elif task == "LANGUAGE":
		ev = ["math", "story"]
	elif task == "MOTOR":
		ev = ["cue", "lf", "lh", "rf", "rh", "t"]
	elif task == "RELATIONAL":
		ev = ["err", "match", "relation"]
	elif task == "SOCIAL":
		ev = ["mental", "mental_resp", "rnd", "rnd_resp"]
	elif task == "WM":
		ev = ["0bk_body", "0bk_cor", "0bk_err", "0bk_faces", "0bk_places", "0bk_tools", "2bk_body" ,"2bk_cor", "2bk_err", "2bk_faces", "2bk_places", "2bk_tools"]
	elif task == "REST1" or task == "REST2":
		ev = []
	else:
		ev = []

	return(ev)


class PATH(object):
	def __init__(self):
		self.lpar = None
		self.lsub = None
		self.spar = None
		self.ssub = None
		return


class ABSPATH(object):
	def __init__(self):
		self.ev_raw = None
		self.ev_proc = None
		self.roi_raw = None
		self.roi_proc = None
		return


class ProjectInfo(object):
	'''
	Default information about this project, including the following variables.


	>>> Variables
	
	- System information
		+ 'pfix': user folder

	- Data information
		+ 'direct': False, directed or undirected connectivity
		+ 'phase': 'LR', phase encoding direction
		+ 'TR': 0.72
		+ 'TE': 0.033
		+ 'unit': 'sec', time unit for TR and TE
		+ 'id_length': 6, length of subject ID, eg. subject 100206 has 6 digits

	- Task information
		+ 'rest_labels': part of `rest_labels_all`
		+ 'rest_labels_all': ['REST1', 'REST2']
		+ 'rest_total_pnts': [1200, 1200]
		+ 'task_labels': part of `task_labels_all`
		+ 'task_labels_all': ['EMOTION', 'GAMBLING', 'LANGUAGE', 'MOTOR', 
			'RELATIONAL', 'SOCIAL', 'WM']
		+ 'task_total_pnts': [176, 253, 316, 284, 232, 274, 405]
	
	- Event information for task fMRI
		+ 'resp_labels': ['cor', 'err', 'event', 'resp']
		+ 'cue_labels': 'cue'
		+ 'stamp_level': 1
			* stamp_level = 1: not including `resp_labels` and `cue_labels`
			* stamp_level = 2: not including `resp_labels`
			* stamp_level = 3: including everything

	- Event padding for task fMRI
		+ 'pad_label': 'zzzzz', label for the resting period at the beginning.
		+ 'pad_value': 99999, numeric label for the resting period at the beginning.
		+ 'del_pad': 1, indicator of whether resting period is deleted or not
		+ 'minlen': 2, minimum length of an event.
		+ 'ndel': 0, number of points deleted at the beginning

	- Binarializing the adjacency matrix: preserved edge density
		+ 'densMax': 0.5
		+ 'densMin': 0.01
		+ 'densPool': np.arange(info['densMin'], info['densMax']+0.01, 0.01)
		+ 'ndens': info['densPool'].shape[0]
		+ 'densStartIdx': 20 # used to be `dStart`
		+ 'densSelectVal': 0.25 # used to be `densSelect`

	- File processing
		+ skip_exist: 0

	- Sliding window
		+ 'wlen': 15
		+ 'shift': 1

	>>> MISC
	'''

	'''
	Data information
	'''
	__TR = 0.72
	__TE = 0.033
	__unit = 'sec'

	'''
	Task information
	'''
	__rest_labels_all = ['REST1', 'REST2']
	__rest_total_pnts = [1200, 1200]
	__task_labels_all = ['EMOTION', 'GAMBLING', 'LANGUAGE', 'MOTOR', 
		'RELATIONAL', 'SOCIAL', 'WM']
	__task_total_pnts = [176, 253, 316, 284, 232, 274, 405]

	'''
	Event information for task fMRI
	'''
	resp_labels = ['cor', 'err', 'event', 'resp']
	cue_labels = 'cue'
	
	'''
	Event padding for task fMRI
	'''
	pad_label = 'rest'
	pad_value = 99999

	'''
	Preserved edge density while binarializing the adjacency matrix
	'''
	densMax = 0.5
	densMin = 0.01
	densPool = np.arange(densMin, densMax+0.01, 0.01)
	ndens = densPool.shape[0]
	densStartIdx = 20 # used to be `dStart`
	densSelectVal = 0.25 # used to be `densSelect`

	'''
	File processing
	- skip_exist = 0, do nothing
	- skip_exist = 1, skip it
	- skip_exist = 2, delete it
	'''

	'''
	Sliding Window
	'''
	wlen = 15
	shift = 1

	def __init__(self, roi_name="AAL90", data_type='tfMRI', phase='LR', dir='/Dropbox/data/'):
		self.direct = False
		self.phase = str(phase)
		self.id_length = 6

		self.rest_labels = self.__rest_labels_all
		self.task_labels = self.__task_labels_all

		self.stamp_level = 1
		self.del_pad = 1
		self.minlen = 2
		self.ndel = 0

		self.skip_exist = 0

		self.__pfix = os.path.expanduser("~")
		self.roi_name = roi_name
		self.data_type = data_type
		self.get_default_path(pattern = dir)

		return

	def update(self, **kwargs):
		for key, value in kwargs.items():
			vtype = type(getattr(self, key))
			if isinstance(value, vtype):
				setattr(self, key, value)
			else:
				print("AttributeError: Data type must be %s." % vtype)
		return

	def get_default_path(self, pattern='/Dropbox/data/'):
		'''
		self.__pfix = "~"
		pattern = "/Dropbox/data/"
		self.data_type = "rfMRI"
		self.roi_name = "AAL90"

		self.pdef.roi_raw = "~/Dropbox/data/rfMRI_roi/AAL90"
		'''
		self.pdef = ABSPATH()
		if self.data_type is not None:
			# setattr(self.pdef, 'roi_raw', 
			# 	self.__pfix + pattern + self.data_type+'_roi/' + self.roi_name)
			# # self.pdef.roi_raw = "~/Dropbox/data/rfMRI_roi/AAL90"

			setattr(self.pdef, 'roi_raw', 
				self.__pfix + pattern + self.data_type)
			# self.pdef.roi_raw = "~/Dropbox/data/rfMRI"
			
			# setattr(self.pdef, 'roi_proc', 
			# 	self.__pfix + pattern + self.data_type + '_roi_mean/' + self.roi_name)
			# # self.pdef.roi_raw = "~/Dropbox/data/rfMRI_roi_mean/AAL90"

			setattr(self.pdef, 'roi_proc', 
				self.__pfix + pattern + self.data_type)
			# self.pdef.roi_raw = "~/Dropbox/data/rfMRI"

			# setattr(self.pdef, 'ev_raw', 
			# 	self.__pfix + pattern + 'tfMRI_ev')
			setattr(self.pdef, 'ev_raw', 
				self.__pfix + pattern + self.data_type)
			# self.pdef.ev_raw = "~/Dropbox/data/tfMRI"
		
			# setattr(self.pdef, 'ev_proc', 
			# 	self.__pfix + pattern +'tfMRI_ev_proc')
			setattr(self.pdef, 'ev_proc', 
				self.__pfix + pattern + self.data_type)
			# self.pdef.ev_proc = "~/Dropbox/data/tfMRI"
		return

	def pfix(self):
		return self.__pfix

	def TR(self):
		return self.__TR

	def TE(self):
		return self.__TE

	def unit(self):
		return self.__unit

	@staticmethod
	def get_labels(data_type):
		if data_type == "tfMRI":
			return ProjectInfo.__task_labels_all
		elif data_type == "rfMRI":
			return ProjectInfo.__rest_labels_all

	def label_orig_idx(self, label):
		if self.data_type == 'rfMRI':
			return self.__rest_labels_all.index(str(label))
		elif self.data_type == 'tfMRI':
			return self.__task_labels_all.index(str(label))

	def task_ntime(self, label):
		if self.data_type == 'rfMRI':
			idx = self.__rest_labels_all.index(str(label))
			ntime = self.__rest_total_pnts[idx]
		elif self.data_type == 'tfMRI':
			idx = self.__task_labels_all.index(str(label))
			ntime = self.__task_total_pnts[idx]

		return ntime


class TaskLoader(ProjectInfo):
	'''
	Task level wrapper. Subclass of `ProjectInfo)


	>>> Public Variabls

	- task


	>>> Public Methods

	- get_default_subdir(self)
	- get_abs_paths(self)
	- update_abs_paths(self, sid)
	- get_filelist(path, return_abs=False, excld=['Store'])
	- get_subid(sublist)
	- check_path(path)

	'''
	def __init__(self, 
			  data_type='tfMRI', task='LANGUAGE', roi_name="AAL90", 
			  phase='LR', dir="/Dropbox/data/"):
		ProjectInfo.__init__(self, 
					   roi_name=roi_name, data_type=data_type, phase=phase, dir=dir)
		self.phase = str(phase).upper()
		self.task = task
		if self.task is not None:
			self.task = str(self.task).upper()
			self.get_default_subdir()
			self.get_abs_paths()
		return
		

	def get_default_subdir(self):
		'''
		Set paths. No input, no output.


		>>> `self.ev_paths`

		An instance of class `utils.PATH`, paths for event data.

		- lpar: absolute dir for raw event data.
			eg. (OLD) '~/data/tfMRI_ev/LANGUAGE'
			eg. (NEW) `~/Dropbox/data/tfMRI/LANGUAGE`
		- lsub: relative subdir for raw event data.
			eg. 'tfMRI_ev_LANGUAGE_LR_' (this is a folder)
		- spar: absolute dir for processed event data
			eg. '~/data/tfMRI_ev_proc'
		- ssub: relative filename for processed event data
			eg. 'tfMRI_ev_proc_LANGUAGE' (filename without format suffix)


		>>> `self.roi_paths`

		An instance of class `utils.PATH`, paths for roi data.

		- lpar: dir for raw roi data.
			eg. '~/data/rfMRI_roi/AAL90/REST1'
		- lsub: None
		- spar: absolute dir for aggregated roi data.
			eg. '~/data/rfMRI_roi_mean/AAL90/REST1'
		- ssub: relative filename for aggregated roi data.
			eg. 'rfMRI_REST1_LR_AAL90_mean_'


		>>> NOTE


		'''
		self.ev_paths = PATH()
		if self.pdef.ev_raw is not None:
			setattr(self.ev_paths, 'lpar', 
				self.pdef.ev_raw + '/' + self.task)
			# self.pdef.ev_raw = "~/Dropbox/data/tfMRI"
			# self.ev_paths.lpar = "~/Dropbox/data/tfMRI/LANGUAGE"
		
		setattr(self.ev_paths, 'lsub', 
		  self.data_type + '_' + self.task + '_' + self.phase + '_')
		# self.ev_paths.lsub = "tfMRI_LANGUAGE_LR_"
		
		if self.pdef.ev_proc is not None:
			setattr(self.ev_paths, 'spar', 
				self.pdef.ev_proc + '/' + self.task)
			# self.ev_proc = "~/Dropbox/data/tfMRI"
			# self.ev_paths.spar = "~/Dropbox/data/tfMRI/LANGUAGE"
		
		# setattr(self.ev_paths, 'ssub', "tfMRI_ev_proc_"+self.task)
		setattr(self.ev_paths, 'ssub', 
		  self.data_type + '_' + self.task + '_' + self.phase + '_event')
		# self.ev_paths.ssub = "tfMRI_LANGUAGE_LR_event"
		
		# self.pdef.roi_raw = "~/Dropbox/data/rfMRI"
		# self.roi_paths.lpar = "~/Dropbox/data/rfMRI/REST1"
		# self.pdef.roi_proc = "~/Dropbox/data/rfMRI"
		# self.roi_paths.spar = "~/Dropbox/data/rfMRI/REST1"
		# self.roi_paths.ssub = "rfMRI_REST1_LR_AAL90_mean_"

		self.roi_paths = PATH()
		if self.pdef.roi_raw is not None:
			setattr(self.roi_paths, 'lpar', 
				self.pdef.roi_raw + '/' + self.task)
		if self.pdef.roi_proc is not None:
			setattr(self.roi_paths, 'spar', 
				self.pdef.roi_proc + '/' + self.task)
		if self.data_type is not None:
			setattr(self.roi_paths, 'ssub', 
				self.data_type+"_" + self.task + "_" + self.phase + "_" + self.roi_name + "_mean_")

		return

	def get_abs_paths(self):
		'''
		Set absolute paths for file reading and writing.
		No input, no output. Assign value to self.paths (instance of `ABSPATH`).

		To get a valid path from `PATH` instances:

		- ev_raw: ev_paths.lpar+'/'+ev_paths.lsub+str(sid) (a folder)
		- ev_proc: ev_paths.spar+'/'+ev_paths.ssub (file)

		- roi_raw: roi_paths.lpar+'/'+str(sid) (a folder)
		- roi_proc: roi_paths.spar+'/'+roi_paths.ssub+str(sid)+'.csv' (file)

		'''
		self.paths = ABSPATH()
		if self.ev_paths.lpar is not None and self.ev_paths.lsub is not None:
			setattr(self.paths, 'ev_raw', 
				self.ev_paths.lpar + '/' + self.ev_paths.lsub)
			# "~/Dropbox/data/tfMRI/LANGUAGE/tfMRI_LANGUAGE_LR_"
			
		if self.ev_paths.spar is not None and self.ev_paths.ssub is not None:
			setattr(self.paths, 'ev_proc', 
				self.ev_paths.spar + '/' + self.ev_paths.ssub)
			# self.ev_paths.spar = "~/Dropbox/data/tfMRI/LANGUAGE"
			# self.ev_paths.ssub = "tfMRI_LANGUAGE_LR_event"
			# self.paths.ev_proc = "~/Dropbox/data/tfMRI/LANGUAGE/tfMRI_LANGUAGE_LR_event"

		if self.roi_paths.lpar is not None:
			setattr(self.paths, 'roi_raw', 
				self.roi_paths.lpar+'/')
		# self.paths.roi_raw = "~/Dropbox/data/rfMRI/REST1/"
		
		if self.roi_paths.spar is not None and self.roi_paths.ssub is not None:
			setattr(self.paths, 'roi_proc', 
				self.roi_paths.spar+'/'+self.roi_paths.ssub)
			# self.paths.roi_proc = "~/Dropbox/data/rfMRI/REST1/rfMRI_REST1_LR_AAL90_mean_"
		return


	@staticmethod
	def update_abs_paths(cls, sid):
		paths = ABSPATH()
		if cls.paths.ev_raw is not None:
			setattr(paths, 'ev_raw', cls.paths.ev_raw + str(sid))
		if cls.paths.ev_proc is not None:
			setattr(paths, 'ev_proc', cls.paths.ev_proc)
		if cls.paths.roi_raw is not None:
			setattr(paths, 'roi_raw', cls.paths.roi_raw + str(sid))
		if cls.paths.roi_proc is not None:
			setattr(paths, 'roi_proc', cls.paths.roi_proc + str(sid)+'.csv')
		return paths

	@staticmethod
	def get_filelist(path, return_abs=False, excld=['Store','tar']):
		'''
		Get filelist.


		>>> REQUIRED INPUT

		- path: str


		>>> OPTIONAL INPUT

		- return_abs: bool
		- excld: str list


		>>> OUTOUT

		- fielist: a list of relative paths.


		>>> USAGE

		- Get raw roi subids: 
			`sublist = get_filelist(self.roi_paths.lpar)`
		- Get raw roi subject roilist: 
			`roilist = get_filelist(self.paths.roi_raw+str(sid), return_abs=True)`
			(for sid in sublist)
		- Get proc roi filelist: 
			`filelist = get_filelist(self.roi_paths.spar)`
		- Get proc roi subids:
			`subids = get_subid(filelist)`
		- Get raw event subids
			`subdirs = get_filelist(self.ev_paths.lpar)`
			`sublist = get_subid(subdirs)`
		- Get 

		'''
		filelist = os.listdir(str(path))
		for pattern in excld:
			filelist = [file for file in filelist if pattern not in file]

		filelist = np.sort(np.asarray(filelist)).tolist()

		if return_abs:
			filelist = [path+'/'+file for file in filelist]

		return filelist

	@staticmethod
	def get_subid(sublist, sorting=False):
		sids = [sdir.split(sep='_')[-1] for sdir in sublist]
		sids = [sid.split(sep='.')[0] for sid in sids]
		sids = [sid for sid in sids if 'Store' not in sid]
		if sorting:
			sids = np.sort(np.asarray(sids)).tolist()

		return sids

	@staticmethod
	def check_path(path):
		if not os.path.exists(path):
			os.makedirs(path)
		return



class EventInfo(TaskLoader):
	'''
	Subclass of `TaskLoader`, information of event(stimulus) sequence for 
	all subjects in one task.


	>>> Public Variables

	- seq: a list of event(stimulus) sequences, numpy 1darray
	- dloc: a list of trimmed location, int64
	- sid: a list of subject id, int64
	- nsub: number of subjects

	>>> Public Methods

	- add
	- select
	- save
	- load
	- seq2mat
	- id2index
	- glm_x
	- get_evlist
	- all_evlist

	'''
	def __init__(self, task='LANGUAGE', data_type="tfMRI", roi_name="AAL90", phase='LR', dir="/Dropbox/data/"):
		TaskLoader.__init__(self, data_type, task, roi_name="AAL90", phase=phase, dir=dir)
		self.phase = str(phase)
		self.fname = "tfMRI_" + self.task + '_' + self.phase + '_event'
		self.nsub = 0
		self.ntime = []
		self.nstate = []
		self.seq = []
		self.mat = []
		self.X = []
		self.dloc = []
		self.sid = []
		return

	def add(self, seq_new, dloc_new, sid_new):
		self.seq.append(np.asarray(seq_new))
		self.dloc.append(int(dloc_new))
		self.sid.append(int(sid_new))
		self.nsub = len(self.sid)

		nstate = np.unique(np.asarray(seq_new)).shape[0]
		ntime = np.asarray(seq_new).shape[0]
		self.nstate.append(nstate)
		self.ntime.append(ntime)
		return

	@staticmethod
	def get_evlist(filepath, cls):
		evlist = os.listdir(filepath)
		resp_flag = []
		cue_flag = []
		for file in evlist:
			resp_flag.append(not any([pat in file for pat in cls.resp_labels]))
			cue_flag.append(cls.cue_labels not in file)

		flag = []
		if cls.stamp_level == 1: # exclude `resp_labels` and `cue_labels`
			for file in evlist:
				flag.append(not any([pat in file for pat in cls.resp_labels]) and cls.cue_labels not in file)
		elif cls.stamp_level == 2: # exclude `resp_labels`
			for file in evlist: 
				flag.append(not any([pat in file for pat in cls.resp_labels]))
		elif cls.stamp_level == 3: # include everything
			for file in evlist:
				flag.append(True)

		evlist = [evlist[i] for i in np.where(flag)[0].astype(int)]
		return evlist

	def all_evlist(self):
		filelist = TaskLoader.get_filelist(self.ev_paths.lpar, return_abs=True)
		evlist = []
		for evpath in filelist:
			evlist.append(EventInfo.get_evlist(evpath, self))
		return evlist

	@staticmethod
	def id2index(sid, id_num):
		try:
			abs_i = np.where(np.asarray(sid).astype(int)==int(id_num))[0][0]
		except IndexError:
			abs_i = None
		return abs_i

	@staticmethod
	def select(cls, ids, dir="/Dropbox/data/"):
		event_new = EventInfo(data_type=cls.task, phase=cls.phase, dir=dir)
		for i in ids:
			abs_i = EventInfo.id2index(cls.sid,i)
			if abs_i is not None:
				event_new.add(cls.seq[abs_i], cls.dloc[abs_i], cls.sid[abs_i])
			else:
				print("Event info about subject {} doesn't exist.".format(i))
		return event_new

	def seq2mat(self, id_num=None):
		self.mat = []
		if id_num is None:
			id_num = self.sid

		for id_selected in id_num:
			abs_i = EventInfo.id2index(self.sid,id_selected)
			state = np.zeros((self.nstate[abs_i], self.ntime[abs_i]))
			for k in range(0,self.nstate[abs_i]):
				sidx = np.where(np.asarray(self.seq[abs_i])==k)[0]
				state[k,sidx] = 1

			self.mat.append(state)

		return

	def glm_x(self, hrf_resp, id_num=None):
		self.X = []
		if id_num is None:
			id_num = self.sid

		for id_selected in id_num:
			abs_i = EventInfo.id2index(self.sid,id_selected)
			# X: ntime x nstate
			X = np.zeros((self.ntime[abs_i], self.nstate[abs_i])) 
			for k in range(0,self.nstate[abs_i]):
				X[:,k] = np.convolve(hrf_resp, self.mat[abs_i][k,:])[:self.ntime[abs_i]]

			self.X.append(X)

		return


	def save(self):
		# self.check_path(('/').join(self.paths.ev_proc.split('/')[:-1]))
		np.savetxt(self.paths.ev_proc + '.ev', np.asarray(self.seq[0]))
		# file = open(self.paths.ev_proc + '.ev', 'wb')
		# pickle.dump(self.seq, file)
		# file.close()
		fpath = self.paths.ev_proc + '_' + str(self.sid[0])
		if (not os.path.isdir(fpath)):
			os.mkdir(fpath)
		
		ev = get_event_info(self.task)
		seq_tmp = np.asarray(self.seq[0]).astype(int)
		cls = np.unique(seq_tmp)
		for i in range(len(cls)):
			ffp = os.path.join(fpath, ev[i] + '.1D')
			tmp = np.asarray(seq_tmp.astype(int) == int(cls[i])).astype(int)
			np.savetxt(ffp, tmp, fmt = '%d')

		info = np.vstack((self.sid,self.dloc)).T.astype(str)
		np.savetxt(self.paths.ev_proc + '.csv', info, fmt='%s,%s')
		return


	def load(self, sid=None):
		# file = open(self.paths.ev_proc + '.ev', 'rb')
		# self.seq = pickle.load(file)
		# file.close()
		ncls = len(np.unique(self.seq))

		self.seq = np.loadtxt(self.paths.ev_proc + '.ev')
		self.seq = self.seq.tolist()

		info = np.genfromtxt(self.paths.ev_proc + '.csv', 
			dtype=None, delimiter=',')
		if (len(info.shape) == 1):
			info = info[np.newaxis, ...]

		self.sid = info[:,0].tolist() # dtype: int64
		self.dloc = info[:,1].tolist() # dtype: int64
		self.nsub = len(self.sid)
		self.nstate = [np.unique(np.asarray(seq)).shape[0] for seq in self.seq]
		self.ntime = [np.asarray(seq).shape[0] for seq in self.seq]
		return


