# ipython --matplotlib
import os,sys
import pandas as pd
import numpy as np
from scipy.io import loadmat

import matplotlib.pyplot as plt
from nilearn import plotting

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

from sklearn.metrics import normalized_mutual_info_score as nmi


# Visualize Community Membership
# 1. Static adjacency matrix across time
# 2. Community Memebership matrix
# 3. 3D node coordinates
# 4. Visualization: plotting.view_markers, plotting.plot_connectome, plotting.view_connectome
# 5. Output: view.save_as_html("surface_plot.html"), view.open_in_browser() 



def load_data(filepath):
    '''
    >>> OUTPUT

    - wcorr: nroi x nroi x nsub
    '''
    if len(filepath.split('.')) == 1:
        # then `filepath` is a folder
        filelist = os.listdir(filepath)
        filelist = [flist for flist in filelist if 'Store' not in flist]
        filelist = [flist for flist in filelist if 'tar.gz' not in flist]
        wcorr = [np.load(filepath+'/'+flist)[:,:,np.newaxis] for flist in filelist]
        wcorr = np.concatenate(wcorr, axis=2)
    else:
        # `filepath` is a file
        wcorr = np.load(filepath)

    return wcorr


def cov2cor(covmat):
    D = np.diag(1/np.sqrt(np.abs(np.diag(covmat))))
    cormat = np.matmul(np.matmul(D,covmat), D)
    return cormat + cormat.T - np.diag(cormat.diagonal())


def cov2pcor(covmat):
    covinv = np.linalg.inv(covmat)
    D = np.diag(1/np.sqrt(np.abs(np.diag(covinv))))
    pcormat = np.matmul(np.matmul(D,covinv), D)
    return pcormat + pcormat.T - np.diag(pcormat.diagonal())


def cluster_nmi(cluster_a, cluster_b):
    ni = cluster_a.shape[1]
    nj = cluster_b.shape[1]
    out_nmi = np.empty((ni,nj))
    for i in range(ni):
        for j in range(nj):
            out_nmi[i,j] = nmi(cluster_a[:,i],cluster_b[:,j])

    return out_nmi



def calc_association(membership):
    """
    Calculate membership association.

    >>> REQUIRED INPUT

    - membership: nroi x nsub x nstate numpy.ndarray of a list of such ndarray, 
        nsub can be 1, nstate can be 1
    """
    if type(membership) is list:
        niter = len(membership)
    elif type(membership) is np.ndarray:
        niter = 1
    else:
        print('Invalid wcorr input: must be list or numpy.ndarray.')
        return

    asso_task = []
    for iter in range(niter):
        if niter == 1:
            mbs = np.asarray(membership)
        else:
            mbs = np.asarray(membership[iter])

        if len(mbs.shape) == 1:
            mbs = mbs[...,np.newaxis,np.newaxis]
        elif len(mbs.shape) == 2:
            mbs = mbs[...,np.newaxis]

        nroi, nsub, nstate = mbs.shape

        asso = np.zeros((nroi,nroi,nsub,nstate))
        for siter in range(nsub):
            for qiter in range(nstate):
                mbbs = mbs[:,siter,qiter]
                for i in range(nroi):
                    asso[i,np.where(mbbs==mbbs[i]),siter,qiter] = 1
                    asso[i,i,siter,qiter] = 0

        asso_task.append(asso)

    return asso_task


def get_coords(roipath):
    coords,codes = plotting.find_parcellation_cut_coords(roipath+'.nii', 
        return_label_names=True)
    ssorder = sorted(range(len(codes)),key=lambda x: codes[x])
    coords = coords[ssorder,:]
    if 'AAL90' in roipath:
        coords = coords[0:90,:]

    roi_info = pd.read_csv(roipath+'.info', sep=',', 
        header=None, usecols=[1,3])
    labels = roi_info.values[:,1]

    return coords,labels


def get_index_from_direction(direction):
    """Returns numerical index from direction
    """
    directions = ['x', 'y', 'z']
    try:
        # l and r are subcases of x
        if direction in 'lr':
            index = 0
        else:
            index = directions.index(direction)
    except ValueError:
        message = (
            '{0} is not a valid direction. '
            "Allowed values are 'l', 'r', 'x', 'y' and 'z'").format(direction)
        raise ValueError(message)
    return index



def coords_3d_to_2d(coords_3d, direction, return_direction=False):
    """Project 3d coordinates into 2d ones given the direction of a cut
    """
    index = get_index_from_direction(direction)
    dimensions = [0, 1, 2]
    dimensions.pop(index)

    if return_direction:
        return coords_3d[:, dimensions], coords_3d[:, index]

    return coords_3d[:, dimensions]


def sorted_unique_community(comvec, ncom=None):
    comunq,comcnt = np.unique(comvec, return_counts=True)
    ssorder = sorted(range(len(comunq)), key=lambda x: comcnt[x], reverse=True)
    comunq = comunq[ssorder]

    if ncom is not None:
        comunq = comunq[:min(ncom,len(comunq))]

    return comunq


def select_community_subset(comvec, wcorr, coords, labels=None, ncom=6):
    # squeeze empty axis
    comvec = np.squeeze(np.asarray(comvec))
    wcorr = np.squeeze(np.asarray(wcorr))
    coords = np.squeeze(np.asarray(coords))

    # sort from biggest community to smallest community
    # only keep the first `ncom`
    comunq = sorted_unique_community(comvec, ncom=ncom) 
    idxkeep = np.where(np.isin(comvec,comunq))[0]

    # slicing
    comvec = np.squeeze(comvec[idxkeep])
    coords = np.squeeze(coords[idxkeep,:])
    wcorr = np.squeeze(wcorr[idxkeep,:][:,idxkeep])

    if labels is not None:
        labels = np.squeeze(np.asarray(labels))[idxkeep]
        return comvec,wcorr,coords,labels
    else:
        return comvec,wcorr,coords


def assign_community_colors(comvec):
    cpalatte = np.asarray(['tomato', 'cyan', 'violet', 'yellow', 'lime', 'brown'])
    cnoplot = 'white'
    
    comunq = sorted_unique_community(comvec, ncom=cpalatte.shape[0])
    comclr = cpalatte[:comunq.shape[0]]

    # assign color to `comvec`
    comcolor = []
    for v in comvec:
        vidx = np.where(np.isin(comunq,v))[0][0]
        if v in comunq:
            comcolor.append(comclr[vidx])
        else:
            comcolor.append(cnoplot)

    comcolor = np.squeeze(np.asarray(comcolor))

    return comcolor


def plot_single_connectome(coords, wcorr, comvec=None, ncom=6, 
    edge_threshold=0.99, interactive=False, display_mode='z',
    fig=None, ax=None):
    """
    Geographically visualize detected communities (nodes), or community 
    association, or community consistency.


    >>> REQUIRED INPUT

    - wcorr: ndarray, nroi x nroi, correlation or association or consistency
    - coords: ndarray, nroi x 3, coordinates of ROIs


    >>> OPTIONAL INPUT

    - comvec: integer ndarray, nroi x 1, community membership matrix
    - ncom: numeric, 1 x 1, number of communities kept in the figure
    - edge_threshold: numeric in [0,1], 1 x 1, percentage of edges kept
    - interactive: logical, 1 x 1
        + interactive == False: plot 2D figures
        + interactive == True: plot 3D figures, cannot visualize communities


    >>> MISC

    AUTHOR: MEINI
    DATE: July 10, 2019
    """
    # thres = np.quantile(wcorr.flatten(), edge_threshold)
    thres_min = np.nanpercentile(wcorr.flatten(),
        edge_threshold*100)
        # color_min = np.quantile(wtmp,0.5)
    color_min = np.nanmin(wcorr.flatten())
    color_max = np.nanmax(wcorr.flatten())

    if interactive==False:
        # 2D
        if comvec is not None:
            # subsetting
            comvec, wcorr, coords = select_community_subset(comvec, wcorr, 
                coords, ncom=ncom)
            comcolor = assign_community_colors(comvec).tolist()
        else:
            comcolor = 'black'
        
        view = plotting.plot_connectome(wcorr, coords, 
            figure=fig, axes=ax, node_color=comcolor, 
            edge_threshold=thres_min, node_size=10, 
            display_mode=display_mode, edge_cmap='PuBu',
            edge_kwargs={'alpha':0.7, 'linewidth':1})
    else:
        # 3D
        view = plotting.view_connectome(wcorr, coords, 
            edge_threshold=thres_min, linewidth=1)


    return view



def plot_single_subnetwork(coords, wcorr, indicator=None, 
    fig=None, axes=None, title=None, title_size=10,
    labels=None, label_alpha=1, label_size=4, label_offset=1.8,
    node_color=None, node_size=20, display_mode='z'):
    """
    Geographically visualize detected communities (nodes), or community 
    association, or community consistency.


    >>> REQUIRED INPUT

    - wcorr: ndarray, nroi x nroi, correlation or association or consistency
    - coords: ndarray, nroi x 3, coordinates of ROIs


    >>> OPTIONAL INPUT

    - comvec: integer ndarray, nroi x 1, community membership matrix
    - ncom: numeric, 1 x 1, number of communities kept in the figure
    - edge_threshold: numeric in [0,1], 1 x 1, percentage of edges kept
    - interactive: logical, 1 x 1
        + interactive == False: plot 2D figures
        + interactive == True: plot 3D figures, cannot visualize communities


    >>> MISC

    AUTHOR: MEINI
    DATE: July 10, 2019
    """
    if indicator is not None: # filter
        indicator = indicator.astype(bool)
        wcorr = wcorr[indicator,:]
        wcorr = wcorr[:,indicator]
        coords = coords[indicator,:]
        labels = labels[indicator]

    N = wcorr.shape[0]
    view = plotting.plot_connectome_strength(wcorr, coords,
        figure=fig, axes=axes,node_size=node_size,
        display_mode=display_mode)
    # region_strength = np.sum(np.abs(adjacency_matrix), axis=0)
    # region_strength /= np.sum(region_strength)

    if labels is not None:
        vkeys = list(view.axes.keys())
        for n in range(N):
            for k in vkeys:
                crd = coords_3d_to_2d(coords, k)
                view.axes[k].ax.annotate(labels[n],crd[n,:],
                    xytext=crd[n,]+label_offset,
                    alpha=label_alpha,fontsize=label_size)

    if title is not None:
        view.title(title,size=title_size)


    return view


def plot_single_LambdaK(LambdaK, coords, labels, threshold=0,
    label_alpha=1, label_size=4, label_offset=1.8, 
    calc_strength=True, normalize=False, removeOne=True,
    node_size="auto",cmap=None, output_file=None, display_mode="ortho",
    figure=None, axes=None, title=None):
    """Plot connectome strength on top of the brain glass schematics.

    The strength of a connection is define as the sum of absolute values of
    the edges arriving to a node.

    Parameters
    ----------
    adjacency_matrix : numpy array of shape (n, n)
        represents the link strengths of the graph. Assumed to be
        a symmetric matrix.
    node_coords : numpy array_like of shape (n, 3)
        3d coordinates of the graph nodes in world space.
    node_size : 'auto' or scalar
        size(s) of the nodes in points^2. By default the size of the node is
        inversely propertionnal to the number of nodes.
    cmap : str or colormap
        colormap used to represent the strength of a node.
    output_file : string, or None, optional
        The name of an image file to export the plot to. Valid extensions
        are .png, .pdf, .svg. If output_file is not None, the plot
        is saved to a file, and the display is closed.
    display_mode : string, optional. Default is 'ortho'.
        Choose the direction of the cuts: 'x' - sagittal, 'y' - coronal,
        'z' - axial, 'l' - sagittal left hemisphere only,
        'r' - sagittal right hemisphere only, 'ortho' - three cuts are
        performed in orthogonal directions. Possible values are: 'ortho',
        'x', 'y', 'z', 'xz', 'yx', 'yz', 'l', 'r', 'lr', 'lzr', 'lyr',
        'lzry', 'lyrz'.
    figure : integer or matplotlib figure, optional
        Matplotlib figure used or its number. If None is given, a
        new figure is created.
    axes : matplotlib axes or 4 tuple of float: (xmin, ymin, width, height), \
optional
        The axes, or the coordinates, in matplotlib figure space,
        of the axes used to display the plot. If None, the complete
        figure is used.
    title : string, optional
        The title displayed on the figure.

    Notes
    -----
    The plotted image should in MNI space for this function to work properly.
    """

    # input validation
    if cmap is None:
        cmap = plt.cm.viridis_r
    elif isinstance(cmap, str):
        cmap = plt.get_cmap(cmap)
    else:
        cmap = cmap

    node_size = (1 / len(coords) * 1e4
                 if node_size == 'auto' else node_size)
    coords = np.asarray(coords)

    # plotting
    if normalize and np.isfinite(np.nanstd(LambdaK)) and np.nanstd(LambdaK)>0:
        LambdaK = (LambdaK - np.nanmean(LambdaK)) / np.nanstd(LambdaK)
        LambdaK = np.abs(LambdaK)

    indicator = (np.abs(LambdaK)>0)
    if removeOne:
        idx_one = np.abs(LambdaK) >= 1
        coordsOne = coords[idx_one,:]
        indicator *= (np.abs(LambdaK)<1)
        if labels is not None:
            labelsOne = labels[idx_one]

    LambdaK = LambdaK[indicator]
    coords = coords[indicator,:]
    if labels is not None:
        labels = labels[indicator]

    if threshold is not None:
        indicator = LambdaK>threshold
        LambdaK = LambdaK[indicator]
        coords = coords[indicator,:]
        if labels is not None:
            labels = labels[indicator]

    if calc_strength:
        region_strength = np.abs(LambdaK)
        region_strength /= np.sum(region_strength)

        region_idx_sorted = np.argsort(region_strength)[::-1]
        strength_sorted = region_strength[region_idx_sorted]
        coords_sorted = coords[region_idx_sorted]
    else:
        coords_sorted = coords
        strength_sorted = np.abs(LambdaK)

    view = plotting.plot_glass_brain(
        None, display_mode=display_mode, 
        figure=figure, axes=axes, title=title
    )

    for coord, region in zip(coords_sorted, strength_sorted):
        if calc_strength:
            color = list(
                cmap((region - strength_sorted.min()) / strength_sorted.max())
            )
            # reduce alpha for the least strong regions
            color[-1] = (
                (region - strength_sorted.min()) *  # noqa: W504
                (1 / (strength_sorted.max() - strength_sorted.min()))
            )
        else:
            color = list(cmap(0.1))
            color[-1] = 0.5

        # make color to be a 2D array
        view.add_markers([coord], marker_color=[color], 
            marker_size=node_size)

    if labels is not None:
        vkeys = list(view.axes.keys())
        for n in range(len(labels)):
            for k in vkeys:
                crd = coords_3d_to_2d(coords, k)
                view.axes[k].ax.annotate(labels[n],crd[n,:],
                    xytext=crd[n,]+label_offset,
                    alpha=label_alpha,fontsize=label_size)

    if removeOne:
        color = list(cmap(1))
        color[-1] = 0.5
        color2 = color
        color2[-1] = 1
        for coord in coordsOne:
            view.add_markers([coord],'rosybrown',node_size,
                alpha=0.5,edgecolors='rosybrown')
        if labelsOne is not None:
            vkeys = list(view.axes.keys())
            for n in range(len(labelsOne)):
                for k in vkeys:
                    crd = coords_3d_to_2d(coordsOne, k)
                    view.axes[k].ax.annotate(labelsOne[n],crd[n,:],
                        xytext=crd[n,]+label_offset,
                        alpha=label_alpha,fontsize=label_size)


    return view



def plot_single_nodes(coords, comvec, ncom=6):
    comvec, wcorr, coords = select_community_subset(comvec, wcorr, 
        coords, ncom=ncom)
    comcolor = assign_community_colors(comvec)
    view = plotting.view_markers(coords, marker_color=np.asarray(comcolor))
    return view


def config_ticks(labels, label_step):
    if labels is None:
        kept_labels = []
        kept_ticks = []
    elif label_step is not None:
        labels = np.asarray(labels)
        kept_labels = labels[range(0, len(labels), label_step)].tolist()
        kept_ticks = np.arange(0, len(labels), label_step).tolist()
    else:
        kept_labels = np.asarray(labels).tolist()
        kept_ticks = np.arange(len(labels)).tolist()

    return kept_ticks, kept_labels


def plot_single_heatmap(wcorr, ax=None, xlabels=None, xtitle=None, 
    ytitle=None, ylabels=None, label_step=15, normalize=False, absolute=False):
    """
    Plot single subject covariance or association matrix, or the 
    averaged matrix
    """
    wcorr = np.squeeze(np.asarray(wcorr))
    # min-max normalize to [0,1]
    if absolute:
        wcorr = np.abs(wcorr)

    if normalize:
        wcorr = (wcorr - np.min(wcorr)) / (np.max(wcorr) - np.min(wcorr))

    xticks, xlabels = config_ticks(xlabels, label_step)
    yticks, ylabels = config_ticks(ylabels, label_step)

    if ax is None:
        fig, ax = plt.figure()

    ax.imshow(wcorr, vmin=np.nanmin(wcorr.flatten()), 
        vmax=np.nanmax(wcorr.flatten()), cmap='jet')

    ax.set_xticks(xticks, minor=False)
    ax.set_xticklabels(xlabels)
    if xtitle is not None:
        ax.set_title(xtitle, {'fontsize':5})

    ax.set_yticks(yticks, minor=False)
    ax.set_yticklabels(ylabels)
    if ytitle is not None:
        ax.set_ylabel(ytitle, {'fontsize':5})

    ax.tick_params(axis='both', which='both', labelsize=3, 
        labelrotation=45, width=0.3, length=0.5)
    ax.set_frame_on(False)

    return ax


def plot_matrices_heatmap(wcorr, ncol=3, height=1.5, width=1.5, 
    labels=None, plot_aver=False, normalize=False, absolute=False):
    """
    Plot multisubject or multitask (or both) covariance/association matrix.
    Rules:
    - The first column has ylabels indicating the specific task
    - The first row has titles indicating the specific subject


    >>> REQUIRED INPUT

    - wcorr: [nstate x nsub x N x N] numeric ndarray
    """
    if plot_aver is True:
        ncolx = ncol + 1 # each col is a subject or subject average
    else:
        ncolx = ncol

    fig,ax = plt.subplots(1,ncolx, figsize=(height,width), dpi=300)

    for cidx in range(ncol):
        xtitle = 'SUB' + str(cidx+1)            
        wttmp = wcorr[:,:,cidx] # nroi x nroi
        ax[cidx] = plot_single_heatmap(wttmp, ax=ax[cidx], 
            xtitle=xtitle, normalize=normalize, absolute=absolute)

    waver = np.mean(wcorr, axis=2)
    xtitle = 'AVERAGE'
    ax[ncol] = plot_single_heatmap(waver, ax=ax[ncol], 
        xtitle=xtitle, normalize=normalize, absolute=absolute)


    return fig, ax


def plot_matrices_histogram(wcorr, ncol=3, height=5, width=1, 
    labels=None, plot_aver=False, fontsize=6):
    """
    Plot multisubject or multitask (or both) covariance/association matrix.
    Rules:
    - The first column has ylabels indicating the specific task
    - The first row has titles indicating the specific subject


    >>> REQUIRED INPUT

    - wcorr: [nstate x nsub x N x N] numeric ndarray
    """
    ncolx = ncol
    if plot_aver is True:
        ncolx = ncolx + 1 # each col is a subject or subject average

    fig,ax = plt.subplots(1,ncolx, figsize=(height,width), dpi=300)

    for cidx in range(ncol):
        xtitle = 'SUB' + str(cidx+1)

        wttmp = wcorr[:,:,cidx].flatten() # nroi x nroi
        ax[cidx].hist(wttmp)
        plt.setp(ax[cidx].get_xticklabels(), fontsize=fontsize)
        plt.setp(ax[cidx].get_yticklabels(), fontsize=fontsize)
        ax[cidx].set_title("Sub {}".format(cidx+1), fontsize=fontsize)

    fig.tight_layout()
    return fig, ax


def plot_matrices_brainnet(wcorr, coords, membership=None, height=5, 
    width=3, ncol=3, ncom=6, labels=None, thres=0, fontsize=6,
    node_size=0.5, linewidth=1, plot_aver=True):
    """
    Plot multisubject or multitask (or both) covariance/association matrix.
    Rules:
    - The first column has ylabels indicating the specific task
    - The first row has titles indicating the specific subject


    >>> REQUIRED INPUT

    - wcorr: [N x N x nsub] numeric ndarray
    - membership: [N x nsub x nstate]
    """
    assert(len(wcorr.shape)==3)
    nsub = wcorr.shape[2]

    if plot_aver:
        fig, ax = plt.subplots(1,ncol+1,figsize=(height,width), dpi=300)
    else:
        fig, ax = plt.subplots(1,ncol,figsize=(height,width), dpi=300)

    for cidx in range(ncol):
        # cidx: index of subject
        wtmp = wcorr[:,:,cidx].squeeze()
        crdtmp = coords
        comcolor = 'gray'

        if membership is not None:
            mtmp = membership[:,cidx].squeeze()
            comvec, wtmp, crdtmp = select_community_subset(mtmp, wtmp, 
                coords, ncom=ncom)
            comcolor = assign_community_colors(comvec).tolist()

        thres_min = np.nanpercentile(wtmp.flatten(),thres*100)
        # color_min = np.quantile(wtmp,0.5)
        color_min = np.nanmin(wtmp.flatten())
        color_max = np.nanmax(wtmp.flatten())

        plotting.plot_connectome(wtmp, crdtmp, figure=fig, 
            axes=ax[cidx], node_color=comcolor, 
            edge_threshold=thres_min, node_size=node_size, 
            display_mode='z', annotate=False, edge_cmap='BuPu',
            edge_vmin=thres_min, edge_vmax=color_max,
            edge_kwargs={'linewidth':linewidth, 'alpha':0.8})
        ax[cidx].set_title("Sub {}".format(cidx+1), fontsize=fontsize)

    if plot_aver:
        waver = np.mean(wcorr, axis=2)
        thres_min = np.nanpercentile(waver.flatten(),thres*100)
        color_min = np.nanmin(waver.flatten())
        color_max = np.nanmax(waver.flatten())
        plotting.plot_connectome(waver, coords, figure=fig, 
            axes=ax[ncol], node_color='gray', 
            edge_threshold=thres_min, node_size=node_size, 
            display_mode='z', annotate=False, edge_cmap='BuPu',
            edge_vmin=thres_min, edge_vmax=color_max,
            edge_kwargs={'linewidth':linewidth, 'alpha':0.8})
        ax[ncol].set_title("AVER", fontsize=fontsize)

    return fig, ax



